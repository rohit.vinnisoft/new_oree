<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Api\Auth\RegisterController@register');
Route::post('login', 'Api\Auth\LoginController@login');
Route::get('category-and-types', 'Api\DefaultController@getCategoryAndType');
Route::get('quiz', 'Api\DefaultController@getQuiz');
Route::get('portfolio-and-other-data', 'Api\DefaultController@getPortfolioAndOtherData');
Route::get('membership-plans', 'Api\DefaultController@getMembershipPlans');
Route::get('membership-plans-benifits/{plan_id}', 'Api\DefaultController@getMembershipPlansBenifits');
Route::get('investor-benifits', 'Api\DefaultController@getInvestorBenifits');
Route::get('board-member-benifits', 'Api\DefaultController@getBoardMemberBenifits');
Route::post('add-card', 'Api\CardController@addCard');
Route::post('social-login', 'Api\Auth\SocialLoginController@socialLogin');
Route::post('check-user', 'Api\Auth\LoginController@checkUser');
Route::post('forgot-password', 'Api\Auth\ForgotPasswordController@forgotPassword');

Route::middleware('auth:sanctum')->group(function() {
    Route::post('add-selected-plan', 'Api\DefaultController@addMembershipPlan');
    Route::post('change-password', 'Api\Auth\ChangePasswordController@changePassword');
    Route::post('logout', 'Api\Auth\LoginController@logout');

    Route::post('search', 'Api\SearchController@search');
    Route::get('add-recent-search/{user_id}', 'Api\SearchController@addRecentSearch');
    Route::get('recent-searchs', 'Api\SearchController@recentSearches');
    Route::post('add-device-token', 'Api\UserController@addUserDeviceToken');
    Route::post('add-deal-comment-reply', 'Api\CommentController@addDealCommentReply');
    Route::get('get-deal-comment-replies/{comment_id}', 'Api\CommentController@getDealCommentReplies');
    Route::get('like-unlike-comment/{comment_id}', 'Api\CommentController@likeUnlikeComment');
    Route::get('like-unlike-comment-reply/{reply_id}', 'Api\CommentController@likeUnlikeCommentReply');

    Route::group(['prefix' => 'startup'], function() {
        Route::get('profile', 'Api\Startup\ProfileController@getProfile');
        Route::post('edit-profile', 'Api\Startup\ProfileController@editProfile');
        Route::post('update-profile-image', 'Api\Startup\ProfileController@updateProfileImage');
        Route::post('update-cover-image', 'Api\Startup\ProfileController@updateCoverImage');
        Route::get('delete-cover-image', 'Api\Startup\ProfileController@deleteCoverImage');

        Route::post('deals', 'Api\Startup\DealController@deals');
        Route::get('deal-details/{deal_id}', 'Api\Startup\DealController@dealDetails');
        Route::post('add-deal', 'Api\Startup\DealController@addDeal');
        Route::post('edit-deal/{deal_id}', 'Api\Startup\DealController@editDeal');
        Route::get('delete-deal/{deal_id}', 'Api\Startup\DealController@deleteDeal');
        Route::get('like-unlike-deal/{deal_id}', 'Api\Startup\DealController@likeUnlikeDeal');
        Route::post('add-deal-comment', 'Api\Startup\DealController@addDealComment');
        Route::post('get-investors', 'Api\ConnectionsController@getInvestors');
        Route::post('get-startups', 'Api\ConnectionsController@getStartUps');

        Route::get('send-connect-request/{connected_user_id}', 'Api\MyNetworkController@sendConnectRequest');
        Route::get('cancel-connect-request/{connected_user_id}', 'Api\MyNetworkController@cancelConnectRequest');
        Route::post('connected-investors', 'Api\MyNetworkController@getConnectedInvestors');
        Route::post('connected-startups', 'Api\MyNetworkController@getConnectedStartups');
        Route::get('invitations', 'Api\MyNetworkController@getInvitations');
        Route::post('accept-reject-request', 'Api\MyNetworkController@acceptRejectRequest');

        Route::get('block-unblock-user/{user_id}', 'Api\SettingController@blockUnblockUser');
        Route::get('blocked-users', 'Api\SettingController@getBlockedUsers');
        Route::post('change-account-status', 'Api\SettingController@changeAccountStatus');

        Route::get('get-invertor-details/{investorId}', 'Api\ConnectionsController@getInvertorDetails');
        Route::get('get-startup-details/{investorId}', 'Api\ConnectionsController@getStartupDetails');

        Route::get('deal-proposals/{type}/{status?}', 'Api\Startup\DealController@getDealproposals');
        Route::get('deal-proposals-details/{deal_proposal_id}', 'Api\Startup\DealController@getDealproposalsDetails');
        Route::post('accept-reject-proposal', 'Api\Startup\DealController@acceptRejectProposal');

        Route::get('notifications', 'Api\NotificationController@notifications');
    });

    Route::group(['prefix' => 'board_member'], function() {
        Route::post('deals', 'Api\BoardMember\DealController@getDeals');
        Route::get('deal-details/{deal_id}', 'Api\BoardMember\DealController@dealDetails');
        Route::post('send-deal-proposal', 'Api\BoardMember\DealController@sendDealProposal');
        Route::get('my-deals', 'Api\BoardMember\DealController@myDeals');
        Route::post('accept-reject-deal', 'Api\BoardMember\DealController@acceptRejectDeal');

        Route::get('interests', 'Api\InterestController@getInterests');
        Route::post('all-interests', 'Api\InterestController@getAllInterests');
        Route::get('get-random-interests', 'Api\InterestController@getRandomInterests');
        Route::post('add-interests', 'Api\InterestController@addInterest');

        Route::get('board-members', 'Api\BoardMember\UserController@getBoardMembers');
        Route::get('board-member-detail/{board_member_id}', 'Api\BoardMember\UserController@getBoardMemberDetails');

        Route::get('profile', 'Api\BoardMember\ProfileController@getProfile');
        Route::post('edit-profile', 'Api\BoardMember\ProfileController@editProfile');
        Route::post('update-profile-image', 'Api\BoardMember\ProfileController@updateProfileImage');
        Route::post('update-cover-image', 'Api\BoardMember\ProfileController@updateCoverImage');
        Route::get('delete-cover-image', 'Api\BoardMember\ProfileController@deleteCoverImage');
    });

    Route::group(['prefix' => 'investor'], function() {
        Route::get('profile', 'Api\Investor\ProfileController@getProfile');
        Route::post('edit-profile', 'Api\Investor\ProfileController@editProfile');
        Route::post('update-profile-image', 'Api\Investor\ProfileController@updateProfileImage');
        Route::post('update-cover-image', 'Api\Investor\ProfileController@updateCoverImage');
        Route::get('delete-cover-image', 'Api\Investor\ProfileController@deleteCoverImage');

        Route::post('get-investors', 'Api\ConnectionsController@getInvestors');
        Route::post('get-startups', 'Api\ConnectionsController@getStartUps');
        Route::get('get-invertor-details/{investorId}', 'Api\ConnectionsController@getInvertorDetails');
        Route::get('get-startup-details/{investorId}', 'Api\ConnectionsController@getStartupDetails');

        Route::get('send-connect-request/{connected_user_id}', 'Api\MyNetworkController@sendConnectRequest');
        Route::get('cancel-connect-request/{connected_user_id}', 'Api\MyNetworkController@cancelConnectRequest');
        Route::post('connected-investors', 'Api\MyNetworkController@getConnectedInvestors');
        Route::post('connected-startups', 'Api\MyNetworkController@getConnectedStartups');
        Route::get('invitations', 'Api\MyNetworkController@getInvitations');
        Route::post('accept-reject-request', 'Api\MyNetworkController@acceptRejectRequest');

        Route::get('interests', 'Api\InterestController@getInterests');
        Route::post('all-interests', 'Api\InterestController@getAllInterests');
        Route::get('get-random-interests', 'Api\InterestController@getRandomInterests');
        Route::post('add-interests', 'Api\InterestController@addInterest');

        Route::get('block-unblock-user/{user_id}', 'Api\SettingController@blockUnblockUser');
        Route::get('blocked-users', 'Api\SettingController@getBlockedUsers');
        Route::post('change-account-status', 'Api\SettingController@changeAccountStatus');

        Route::post('deals', 'Api\Investor\DealController@deals');
        Route::get('deal-details/{deal_id}', 'Api\Investor\DealController@dealDetails');
        Route::get('stage-one-deal/{deal_id}', 'Api\Investor\DealController@stageOneDeal');
        Route::get('stage-third-deal/{deal_id}', 'Api\Investor\DealController@stageThirdDeal');
        Route::post('send-deal-proposal', 'Api\Investor\DealController@sendDealProposal');
        Route::get('my-deals', 'Api\Investor\DealController@myDeals');

        Route::get('notifications', 'Api\NotificationController@notifications');
    });
});

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
