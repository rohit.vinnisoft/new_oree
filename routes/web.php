<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin');
});

Route::get('change-password/{token}', 'ChangePasswordController@changePassword')->name('change.password');
Route::post('update-password', 'ChangePasswordController@updatePassword')->name('update.password');

Auth::routes();

Route::middleware(['auth', 'is_admin'])->group(function () {
    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/', [App\Http\Controllers\DashboardController::class, 'index'])->name('index');

        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
            Route::get('/{role}', 'UserController@index')->name('index');
            Route::get('/create', 'UserController@create')->name('create');
            Route::get('/{role}/{id}/show', 'UserController@show')->name('show');
            Route::post('/', 'UserController@store')->name('store');
            Route::get('/{id}/edit', 'UserController@edit')->name('edit');
            Route::post('/{id}', 'UserController@update')->name('update');
            Route::get('/delete/{id}', 'UserController@destroy')->name('destroy');
            Route::get('/approve/user/{id}', 'UserController@approveUser')->name('approve.user');
            Route::get('/approve/board-member/{id}', 'UserController@approveAsBoardMember')->name('approve.board-member');
        });

        Route::group(['prefix' => 'attributes', 'as' => 'attributes.'], function () {
            Route::group(['prefix' => 'business-category', 'as' => 'business-category.'], function () {
                Route::get('/', 'BusinessCategoryController@index')->name('index');
                Route::get('/create', 'BusinessCategoryController@create')->name('create');
                Route::post('/', 'BusinessCategoryController@store')->name('store');
                Route::get('/{id}/edit', 'BusinessCategoryController@edit')->name('edit');
                Route::post('/{id}', 'BusinessCategoryController@update')->name('update');
                Route::get('/delete/{id}', 'BusinessCategoryController@destroy')->name('destroy');
            });

            Route::group(['prefix' => 'business-type', 'as' => 'business-type.'], function () {
                Route::get('/', 'BusinessTypeController@index')->name('index');
                Route::get('/create', 'BusinessTypeController@create')->name('create');
                Route::post('/', 'BusinessTypeController@store')->name('store');
                Route::get('/{id}/edit', 'BusinessTypeController@edit')->name('edit');
                Route::post('/{id}', 'BusinessTypeController@update')->name('update');
                Route::get('/delete/{id}', 'BusinessTypeController@destroy')->name('destroy');
            });

            Route::group(['prefix' => 'funding-type', 'as' => 'funding-type.'], function () {
                Route::get('/', 'FundingTypeController@index')->name('index');
                Route::get('/create', 'FundingTypeController@create')->name('create');
                Route::post('/', 'FundingTypeController@store')->name('store');
                Route::get('/{id}/edit', 'FundingTypeController@edit')->name('edit');
                Route::post('/{id}', 'FundingTypeController@update')->name('update');
                Route::get('/delete/{id}', 'FundingTypeController@destroy')->name('destroy');
            });

            Route::group(['prefix' => 'quiz', 'as' => 'quiz.'], function () {
                Route::get('/', 'QuizController@index')->name('index');
                Route::get('/create', 'QuizController@create')->name('create');
                Route::post('/', 'QuizController@store')->name('store');
                Route::get('/{id}/edit', 'QuizController@edit')->name('edit');
                Route::post('/{id}', 'QuizController@update')->name('update');
                Route::get('/delete/{id}', 'QuizController@destroy')->name('destroy');
            });

            Route::group(['prefix' => 'portfolio', 'as' => 'portfolio.'], function () {
                Route::get('/', 'PortfolioController@index')->name('index');
                Route::get('/create', 'PortfolioController@create')->name('create');
                Route::post('/', 'PortfolioController@store')->name('store');
                Route::get('/{id}/edit', 'PortfolioController@edit')->name('edit');
                Route::post('/{id}', 'PortfolioController@update')->name('update');
                Route::get('/delete/{id}', 'PortfolioController@destroy')->name('destroy');
            });

            Route::group(['prefix' => 'industry', 'as' => 'industry.'], function () {
                Route::get('/', 'IndustryController@index')->name('index');
                Route::get('/create', 'IndustryController@create')->name('create');
                Route::post('/', 'IndustryController@store')->name('store');
                Route::get('/{id}/edit', 'IndustryController@edit')->name('edit');
                Route::post('/{id}', 'IndustryController@update')->name('update');
                Route::get('/delete/{id}', 'IndustryController@destroy')->name('destroy');
            });

            Route::group(['prefix' => 'specialty', 'as' => 'specialty.'], function () {
                Route::get('/', 'SpecialtyController@index')->name('index');
                Route::get('/create', 'SpecialtyController@create')->name('create');
                Route::post('/', 'SpecialtyController@store')->name('store');
                Route::get('/{id}/edit', 'SpecialtyController@edit')->name('edit');
                Route::post('/{id}', 'SpecialtyController@update')->name('update');
                Route::get('/delete/{id}', 'SpecialtyController@destroy')->name('destroy');
            });

            Route::group(['prefix' => 'membership-plans', 'as' => 'membership-plans.'], function () {
                Route::get('/', 'MembershipController@index')->name('index');
                Route::get('/create', 'MembershipController@create')->name('create');
                Route::post('/', 'MembershipController@store')->name('store');
                Route::get('/{id}/edit', 'MembershipController@edit')->name('edit');
                Route::post('/{id}', 'MembershipController@update')->name('update');
                Route::get('/delete/{id}', 'MembershipController@destroy')->name('destroy');
            });

            Route::group(['prefix' => 'investor-benefits', 'as' => 'investor-benefits.'], function () {
                Route::get('/', 'InvestorBenefitsController@index')->name('index');
                Route::get('/create', 'InvestorBenefitsController@create')->name('create');
                Route::post('/', 'InvestorBenefitsController@store')->name('store');
                Route::get('/{id}/edit', 'InvestorBenefitsController@edit')->name('edit');
                Route::post('/{id}', 'InvestorBenefitsController@update')->name('update');
                Route::get('/delete/{id}', 'InvestorBenefitsController@destroy')->name('destroy');
            });

            Route::group(['prefix' => 'board-member-benefits', 'as' => 'board-member-benefits.'], function () {
                Route::get('/', 'BoardMemberBenefitsController@index')->name('index');
                Route::get('/create', 'BoardMemberBenefitsController@create')->name('create');
                Route::post('/', 'BoardMemberBenefitsController@store')->name('store');
                Route::get('/{id}/edit', 'BoardMemberBenefitsController@edit')->name('edit');
                Route::post('/{id}', 'BoardMemberBenefitsController@update')->name('update');
                Route::get('/delete/{id}', 'BoardMemberBenefitsController@destroy')->name('destroy');
            });
        });

        Route::group(['prefix' => 'interests', 'as' => 'interests.'], function () {
          Route::get('/', 'InterestController@index')->name('index');
          Route::get('/create', 'InterestController@create')->name('create');
          Route::post('/', 'InterestController@store')->name('store');
          Route::get('/{id}/edit', 'InterestController@edit')->name('edit');
          Route::post('/{id}', 'InterestController@update')->name('update');
          Route::get('/delete/{id}', 'InterestController@destroy')->name('destroy');
        });

        Route::group(['prefix' => 'deals', 'as' => 'deals.'], function () {
            Route::get('/', 'DealController@index')->name('index');
            Route::get('/{id}', 'DealController@show')->name('show');
            Route::get('/delete/{id}', 'DealController@destroy')->name('destroy');
            Route::get('approve/deal/{id}', 'DealController@approveDeal')->name('approve.deal');
            Route::post('reject/deal/{id}', 'DealController@rejectDeal')->name('reject.deal');
        });
    });
});

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/command', function() {
//     $exitCode = Artisan::call('migrate');
// });
