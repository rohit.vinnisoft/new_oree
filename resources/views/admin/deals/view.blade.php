@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="d-flex">
                        <a href="{{ route('admin.deals.index') }}" class="btn btn-dark ms-2">
                            Back
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Deal</h4>
            </div>
            @if (session('success'))
                <div class="alert alert-dark">
                    <span class="text-white">{{ session('success') }}</span>
                </div>
            @endif
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>User Name:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ optional($deal->user)->name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Deal Name:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $deal->name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Business Category:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ optional($deal->businessCategory)->name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Business Type:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ optional($deal->businessType)->name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Description:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $deal->description }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Estimate Profit:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $deal->estimate_profit }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Funding Amount:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $deal->funding_amount }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Equity Percentage:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $deal->equity }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Estimate Revenue:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $deal->estimate_revenue }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Funding Goal:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $deal->funding_goal }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Funding Type:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ optional($deal->fundingType)->name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Documents:</strong>
                        </div>
                        <div class="col-md-9">
                            <a href="{{ $deal->doc }}" target="_blank">Click here to view doc</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 offset-md-3">
                            @if ($deal->admin_approval)
                                @if ($deal->admin_approval == 1)
                                    <strong>Deal Approved</strong>
                                @else
                                    <strong>Deal Rejected</strong>
                                @endif
                            @else
                                <a href="{{ route('admin.deals.approve.deal', ['id' => $deal->id]) }}" class="btn btn-outline-dark">Approve Deal</a>
                                <a href="javascript:void(0)" class="btn btn-outline-danger reject-deal">Reject Deal</a>
                                <form class="" action="{{ route('admin.deals.reject.deal', ['id' => $deal->id]) }}" method="post" id="rejection_form">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="rejection_reason" id="rejection_reason" value="">
                                </form>
                            @endif
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

</div>
@endsection

@section('extra_script')
<script type="text/javascript">
$('.reject-deal').on('click', function() {
    swal({
        text: `Reason of rejection.`,
        icon: "info",
        buttons: ["Cancel", "Add"],
        content: "input",
    })
    .then((value) => {
        if (value) {
            $('#rejection_reason').val(value);
            $('#rejection_form').submit();
        }
    });
});
</script>
@endsection
