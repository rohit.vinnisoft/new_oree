@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                </div>
                <h4 class="page-title">Deals</h4>
            </div>
            @if (session('success'))
                <div class="alert alert-dark">
                    <span class="text-white">{{ session('success') }}</span>
                </div>
            @endif
            <div class="">

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-centered datatable">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>User Name</th>
                                    <th>Deal Name</th>
                                    <th>Business Category</th>
                                    <th>Status</th>
                                    <!-- <th>Description</th>
                                    <th>Business Type</th>
                                    <th>Estimate Profit</th>
                                    <th>Funding Amount</th>
                                    <th>Funding Goal</th>
                                    <th>Funding Type</th> -->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($deals as $key => $deal)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ optional($deal->user)->name }}</td>
                                        <td>{{ $deal->name }}</td>
                                        <td>{{ optional($deal->businessCategory)->name }}</td>
                                        <td>
                                            @if ($deal->admin_approval)
                                                @if ($deal->admin_approval == 1)
                                                    <span class="badge bg-success">Approved</span>
                                                @else
                                                    <span class="badge bg-danger">Rejected</span>
                                                @endif
                                            @else
                                                <span class="badge bg-warning">Not Approved</span><br>
                                            @endif
                                        </td>
                                        <!-- <td>{{ $deal->description }}</td>
                                        <td>{{ optional($deal->businessType)->name }}</td>
                                        <td>{{ $deal->estimate_profit }}</td>
                                        <td>{{ $deal->funding_amount }}</td>
                                        <td>{{ $deal->funding_goal }}</td>
                                        <td>{{ optional($deal->fundingType)->name }}</td> -->
                                        <td>
                                            <a href="{{ route('admin.deals.show', ['id' => $deal->id]) }}" class="btn btn-outline-dark btn-sm">View</a>
                                            <a href="javascript:void(0)" data-name="Deal" data-href="{{ route('admin.deals.destroy', ['id' => $deal->id]) }}" class="btn btn-dark btn-sm confirm-delete">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

</div>
@endsection

@section('extra_script')
<script type="text/javascript">
// swal({
//   title: "Are you sure?",
//   text: "Once deleted, you will not be able to recover this imaginary file!",
//   icon: "warning",
//   buttons: true,
//   dangerMode: true,
// })
// .then((willDelete) => {
//   if (willDelete) {
//     swal("Poof! Your imaginary file has been deleted!", {
//       icon: "success",
//     });
//   } else {
//     swal("Your imaginary file is safe!");
//   }
// });
</script>
@endsection
