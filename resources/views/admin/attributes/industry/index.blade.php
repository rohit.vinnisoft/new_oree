@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="d-flex">
                        <a href="{{ route('admin.attributes.industry.create') }}" class="btn btn-primary ms-2">
                            Create Industry
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Industry</h4>
            </div>
            @if (session('success'))
                <div class="alert alert-dark">
                    <span class="text-white">{{ session('success') }}</span>
                </div>
            @endif
            <div class="">

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-centered datatable">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($industries as $key => $industry)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $industry->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.attributes.industry.edit', ['id' => $industry->id]) }}" class="btn btn-outline-dark btn-sm">Edit</a>
                                        <a href="javascript:void(0)" data-name="Industry" data-href="{{ route('admin.attributes.industry.destroy', ['id' => $industry->id]) }}" class="btn btn-dark btn-sm confirm-delete">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

</div>
@endsection
