@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="d-flex">
                        <a href="{{ route('admin.attributes.investor-benefits.create') }}" class="btn btn-primary ms-2">
                            Create Investor Benefit
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Investor Benefits</h4>
            </div>
            @if (session('success'))
                <div class="alert alert-dark">
                    <span class="text-white">{{ session('success') }}</span>
                </div>
            @endif
            <div class="">

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-centered datatable">
                            <thead>
                                <tr>
                                    <th width="5%">S.No.</th>
                                    <th width="70%">Title</th>
                                    <th width="25%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($investorBenefits as $key => $investorBenefit)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $investorBenefit->title }}</td>
                                    <td>
                                        <a href="{{ route('admin.attributes.investor-benefits.edit', ['id' => $investorBenefit->id]) }}" class="btn btn-outline-dark btn-sm">Edit</a>
                                        <a href="javascript:void(0)" data-name="Investor Benefit" data-href="{{ route('admin.attributes.investor-benefits.destroy', ['id' => $investorBenefit->id]) }}" class="btn btn-dark btn-sm confirm-delete">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

</div>
@endsection
