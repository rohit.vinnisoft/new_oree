@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="d-flex">
                        <a href="{{ route('admin.attributes.membership-plans.index') }}" class="btn btn-primary ms-2">
                            Back
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Create Membership Plan</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.attributes.membership-plans.store') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="mb-3">
                            <label for="name" class="form-label">Plan Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Plan Name" value="{{ old('name') }}" required>
                            @error('name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="monthly_rate" class="form-label">Monthly Rate</label>
                            <input type="text" class="form-control" name="monthly_rate" id="monthly_rate" placeholder="Enter Monthly Rate" value="{{ old('monthly_rate') }}" required>
                            @error('monthly_rate')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="yearly_rate" class="form-label">Yearly Rate</label>
                            <input type="text" class="form-control" name="yearly_rate" id="yearly_rate" placeholder="Enter Yearly Rate" value="{{ old('yearly_rate') }}" required>
                            @error('yearly_rate')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="">
                            <div class="row">
                                <div class="col-10">
                                    <label for="benifits" class="form-label">Benifits</label>
                                </div>
                                <div class="col-2">
                                    <label for="" class="form-label">Add / Remove</label>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="row">
                                <div class="col-10">
                                    <input type="text" class="form-control" name="benifits[]" id="benifits" placeholder="Enter Benifits" value="{{ old('benifits') }}" required>
                                </div>
                                <div class="col-2">
                                    <div class="row">
                                        <div class="col-6">
                                            <button type="button" class="btn btn-info form-control" onclick="addMore()" name="button"><i class="uil-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="new-section"></div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

</div>

<div class="extra-section" hidden>
    <div class="mb-3">
        <div class="row">
            <div class="col-10">
                <input type="text" class="form-control" name="benifits[]" id="benifits" placeholder="Enter Benifits" value="{{ old('benifits') }}" required>
            </div>
            <div class="col-2">
                <div class="row">
                    <div class="col-6">
                        <button type="button" class="btn btn-info form-control" onclick="addMore()" name="button"><i class="uil-plus"></i></button>
                    </div>
                    <div class="col-6">
                        <button type="button" class="btn btn-danger form-control" onclick="removeRow(this)" name="button"><i class="uil-multiply"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra_script')
<script type="text/javascript">
function addMore() {
    var _html = $('.extra-section').html();
    $('.new-section').append(_html);
}

function removeRow(element) {
    var test = element.parentElement.parentElement.parentElement.parentElement;
    test.remove();
}
</script>
@endsection
