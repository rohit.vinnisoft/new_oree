@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="d-flex">
                        <a href="{{ route('admin.interests.create') }}" class="btn btn-primary ms-2">
                            Create Interest
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Interests</h4>
            </div>
            @if (session('success'))
                <div class="alert alert-dark">
                    <span class="text-white">{{ session('success') }}</span>
                </div>
            @endif
            <div class="">

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-centered datatable">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($interests as $key => $interest)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $interest->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.interests.edit', ['id' => $interest->id]) }}" class="btn btn-outline-dark btn-sm">Edit</a>
                                        <a href="javascript:void(0)" data-name="Interests" data-href="{{ route('admin.interests.destroy', ['id' => $interest->id]) }}" class="btn btn-dark btn-sm confirm-delete">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

</div>
@endsection
