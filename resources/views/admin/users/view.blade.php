@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="d-flex">
                        <a href="{{ route('admin.users.index', ['role' => $role]) }}" class="btn btn-dark ms-2">
                            Back
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Users</h4>
            </div>
            @if (session('success'))
                <div class="alert alert-dark">
                    <span class="text-white">{{ session('success') }}</span>
                </div>
            @endif
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>First Name:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $user->first_name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Last Name:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $user->last_name }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-md-end mb-2">
                            <strong>Email:</strong>
                        </div>
                        <div class="col-md-9">
                            {{ $user->email }}
                        </div>
                    </div>
                    @if ($role == 'investor')
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Business Category:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ optional($investor->businessCategory)->name }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Business Type:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ optional($investor->businessType)->name }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Funding Type:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ optional($investor->fundingType)->name }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Preferred valuation range:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ $investor->preferred_valuation_range }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Qualified Investor:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ $investor->qualified_investor ? 'Yes' : 'No' }}
                            </div>
                        </div>
                    @endif
                    @if ($role == 'board_member' || $user->is_board_member)
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Portfolio:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ optional($boardMember->portfolio)->name }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Industry:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ optional($boardMember->industry)->name }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Specialty:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ optional($boardMember->specialty)->name }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Goal:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ $boardMember->goal }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Biggest Professional Success:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ $boardMember->biggest_professional_success }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Biggest Failure:</strong>
                            </div>
                            <div class="col-md-9">
                                {{ $boardMember->biggest_failure }}
                            </div>
                        </div>
                    @endif
                    @if ($user->is_board_member || $user->role == 'investor')
                        <div class="row mb-2">
                            <div class="col-md-3 text-md-end mb-2">
                                <strong>Documents:</strong>
                            </div>
                            <div class="col-md-9">
                                @forelse($user->investorDocuments as $boardMemberDocument)
                                    <a href="{{ '/storage'. $boardMemberDocument->file_path }}" download="{{ $boardMemberDocument->file_name }}">{{ $boardMemberDocument->file_name }}</a><br>
                                @empty
                                    N/A
                                @endforelse
                            </div>
                        </div>
                    @endif
                    @if (in_array($role, ['investor', 'board_member']))
                    <div class="row">
                        <div class="col-md-9 offset-md-3">
                            @if ($user->is_approved && !$user->is_qualified_investor && !$user->is_board_member)
                                <strong>Approved</strong>
                            @elseif ($user->is_approved && $user->role == 'investor')
                                <strong>Approved as Qualified Investor</strong>
                            @elseif ($user->is_approved && $user->role == 'board_member')
                                <strong>Approved as Board Member</strong>
                            @elseif ($user->role == 'investor' && !$user->is_approved)
                                @if ($user->is_qualified_investor)
                                    <a href="{{ route('admin.users.approve.user', ['id' => $user->id]) }}" class="btn btn-outline-dark">Approve as Qualified Investor</a>
                                @endif
                                @if ($user->is_board_member)
                                    <a href="{{ route('admin.users.approve.board-member', ['id' => $user->id]) }}" class="btn btn-outline-dark">Approve as Board Member</a>
                                @endif
                            @endif
                        </div>
                    </div>
                    @endif
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

</div>
@endsection
