@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="d-flex">
                        <a href="{{ route('admin.users.index') }}" class="btn btn-primary ms-2">
                            Back
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Users</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.users.store') }}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="mb-2">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="first_name" class="form-label">First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter first name" value="{{ old('first_name') }}">
                                    @error('first_name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="last_name" class="form-label">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter last name" value="{{ old('last_name') }}">
                                    @error('last_name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="mb-2">
                            <label for="exampleInputEmail1" class="form-label">Email</label>
                            <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Enter email" value="{{ old('email') }}" autocomplete="off" />
                            @error('email')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <label for="password" class="form-label">Password</label>
                            <div class="input-group input-group-merge">
                                <input type="password" id="password" name="password" class="form-control" placeholder="Enter your password">
                                <div class="input-group-text" data-password="false">
                                    <span class="password-eye"></span>
                                </div>
                            </div>
                            @error('password')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <label for="role" class="form-label">Role</label>
                            @php
                                $roles = ['board_member', 'investor', 'start_up'];
                            @endphp
                            <select class="form-select" name="role">
                                <option value="" selected disabled>Select Role</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role }}" {{ (old('role') == $role) ? 'selected' : '' }}>{{ ucwords(str_replace('_', ' ', $role)) }}</option>
                                @endforeach
                            </select>
                            @error('role')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <label for="business_category" class="form-label">Business Category</label>
                            <select class="form-select" name="business_category">
                                <option value="" selected disabled>Select Business Category</option>
                                @foreach ($businessCategories as $businessCategory)
                                    <option value="{{ $businessCategory->id }}" {{ (old('business_category') == $businessCategory->id) ? 'selected' : '' }}>{{ $businessCategory->name }}</option>
                                @endforeach
                            </select>
                            @error('business_category')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <label for="business_type" class="form-label">Business Type</label>
                            <select class="form-select" name="business_type">
                                <option value="" selected disabled>Select Business Type</option>
                                @foreach ($businessTypes as $businessType)
                                    <option value="{{ $businessType->id }}" {{ (old('business_type') == $businessType->id) ? 'selected' : '' }}>{{ $businessType->name }}</option>
                                @endforeach
                            </select>
                            @error('business_type')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <label for="funding_type" class="form-label">Funding Type</label>
                            <select class="form-select" name="funding_type">
                                <option value="" selected disabled>Select Funding Type</option>
                                @foreach ($fundingTypes as $fundingType)
                                    <option value="{{ $fundingType->id }}" {{ (old('funding_type') == $fundingType->id) ? 'selected' : '' }}>{{ $fundingType->name }}</option>
                                @endforeach
                            </select>
                            @error('funding_type')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <label for="preferred_valuation_range" class="form-label">Preferred Valuation Range</label>
                            <input type="text" class="form-control" name="preferred_valuation_range" value="{{ old('preferred_valuation_range') }}" placeholder="Enter Preferred Valuation Range">
                            @error('preferred_valuation_range')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <label for="files" class="form-label">Files</label>
                            <input type="file" class="form-control" name="files[]" value="{{ old('files') }}" multiple>
                            @error('files')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-2">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="qualified_investor" name="qualified_investor" value="{{ old('qualified_investor') }}">
                                <label class="form-check-label" for="qualified_investor">Qualified Investor</label>
                            </div>
                            @error('qualified_investor')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

</div>
@endsection
