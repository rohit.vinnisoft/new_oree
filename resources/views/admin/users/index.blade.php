@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    {{--<form class="d-flex">
                        <a href="{{ route('admin.users.create') }}" class="btn btn-primary ms-2">
                            Create user
                        </a>
                    </form>--}}
                </div>
                <h3 class="page-title">{{ ucwords(str_replace('_', ' ', $role)) }}s</h3>
            </div>
            @if (session('success'))
                <div class="alert alert-dark">
                    <span class="text-white">{{ session('success') }}</span>
                </div>
            @endif
            <div class="">

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-centered datatable">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    @if ($role != 'start_up')
                                        <th>Status</th>
                                    @endif
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $user)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    @if ($role != 'start_up')
                                    <td>
                                        @if ($user->is_approved)
                                            <span class="badge bg-success">Approved</span>
                                        @endif
                                        @if ($user->role == 'investor' && !$user->is_approved)
                                            @if ($user->is_qualified_investor)
                                                <span class="badge bg-warning">Request for Qualified Investor</span><br>
                                            @endif
                                            @if ($user->is_board_member)
                                                <span class="badge bg-warning">Request for Board Member</span>
                                            @endif
                                        @endif
                                        {{--@if ($role == 'investor' && $user->is_board_member)
                                            <span class="badge bg-warning">Request for Board Member</span>
                                        @endif--}}
                                    </td>
                                    @endif
                                    <td>
                                        {{--<a href="{{ route('admin.users.edit', ['id' => $user->id]) }}" class="btn btn-success btn-sm">Edit</a>--}}
                                        <a href="{{ route('admin.users.show', ['role' => $user->role, 'id' => $user->id]) }}" class="btn btn-outline-dark btn-sm">View</a>
                                        <a href="javascript:void(0)" data-name="{{ ucwords(str_replace('_', ' ', $user->role)) }}" data-href="{{ route('admin.users.destroy', ['id' => $user->id]) }}" class="btn btn-dark btn-sm confirm-delete">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

</div>
@endsection
