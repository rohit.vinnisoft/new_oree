<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'profile_image',
        'login_type',
        'social_provider',
        'social_id',
        'deviceType',
        'longitude',
        'latitude',
        'membership_id',
        'membership_type',
        'company_name',
        'is_approved',
        'first_name',
        'last_name',
        'description',
        'cover_image',
        'is_private',
        'device_token',
        'date_of_birth',
        'country',
        'state',
        'city',
        'zip_code',
        'is_board_member'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function investorDocuments()
    {
        return $this->hasMany(InvestorDocument::class);
    }

    public function boardMemberDocuments()
    {
        return $this->hasMany(BoardMemberDocument::class);
    }

    public function getProfileImageAttribute($value)
    {
        return $value ? env('APP_URL') . '/storage' . $value : NULL;
    }

    public function getCoverImageAttribute($value)
    {
        return $value ? env('APP_URL') . '/storage' . $value : NULL;
    }

    public function interests()
    {
        return $this->hasMany(Interest::class);
    }

    public function boardMembers()
    {
        return $this->hasMany(BoardMember::class);
    }
}
