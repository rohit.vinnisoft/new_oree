<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function businessCategory()
    {
        return $this->belongsTo(BusinessCategory::class);
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class);
    }

    public function fundingType()
    {
        return $this->belongsTo(FundingType::class);
    }
}
