<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function businessCategory()
    {
        return $this->belongsTo(BusinessCategory::class, 'business_category_id', 'id');
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class, 'business_type_id', 'id');
    }

    public function fundingType()
    {
        return $this->belongsTo(FundingType::class, 'funding_type_id', 'id');
    }

    public function getDocAttribute($value)
    {
        return $value ? env('APP_URL') . '/storage' . $value : '';
    }
}
