<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BoardMemberBenefit;

class BoardMemberBenefitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boardMemberBenefits = BoardMemberBenefit::all();

        return view('admin.attributes.board-member-benefits.index', compact('boardMemberBenefits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.attributes.board-member-benefits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $boardMemberBenefit = BoardMemberBenefit::create([
            'title' => $request->title
        ]);

        return redirect()->route('admin.attributes.board-member-benefits.index')->with('success', 'Board Member Benefit created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $boardMemberBenefit = BoardMemberBenefit::where('id', $id)->first();

        return view('admin.attributes.board-member-benefits.edit', compact('boardMemberBenefit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $boardMemberBenefit = BoardMemberBenefit::where('id', $id)->update([
            'title' => $request->title
        ]);

        return redirect()->route('admin.attributes.board-member-benefits.index')->with('success', 'Board Member Benefit updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $boardMemberBenefit = BoardMemberBenefit::where('id', $id)->first();
        $boardMemberBenefit->delete();

        return back()->with('success', 'Board Member Benefit deleted successfully.');
    }
}
