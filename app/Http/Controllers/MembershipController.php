<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Membership;

class MembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $membershipPlans = Membership::all();

        return view('admin.attributes.membership-plans.index', compact('membershipPlans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.attributes.membership-plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'         => 'required',
            'monthly_rate' => 'required',
            'yearly_rate'  => 'required'
        ]);

        Membership::create([
            'name'         => $request->name,
            'monthly_rate' => $request->monthly_rate,
            'yearly_rate'  => $request->yearly_rate,
            'benifits'     => json_encode($request->benifits),
        ]);

        return redirect()->route('admin.attributes.membership-plans.index')->with('success', 'Membership Plan added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $membershipPlan = Membership::where('id', $id)->first();
        $benifits       = json_decode($membershipPlan->benifits);

        return view('admin.attributes.membership-plans.edit', compact('membershipPlan', 'benifits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'         => 'required',
            'monthly_rate' => 'required',
            'yearly_rate'  => 'required'
        ]);

        Membership::where('id', $id)->update([
            'name'         => $request->name,
            'monthly_rate' => $request->monthly_rate,
            'yearly_rate'  => $request->yearly_rate,
            'benifits'     => json_encode($request->benifits),
        ]);

        return redirect()->route('admin.attributes.membership-plans.index')->with('success', 'Membership Plan updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $membershipPlan = Membership::where('id', $id)->delete();

        return back()->with('success', 'Membership Plan deleted successfully');
    }
}
