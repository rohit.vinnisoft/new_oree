<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Interest;
use App\Models\UserInterest;
use Auth;
use Validator;

class InterestController extends Controller
{
    public function getAllInterests(Request $request)
    {
        $allInterests = new Interest;
        if ($request->search) {
            $allInterests = $allInterests->where('name', 'like', $request->search."%");
        }
        $allInterests = $allInterests->get();

        return response()->json([
            'success' => true,
            'message' => 'All interests',
            'data'    => $allInterests
        ]);
    }

    public function getRandomInterests()
    {
        $randonInterests = Interest::inRandomOrder()
                                  ->limit(5)
                                  ->get();

        return response()->json([
            'success' => true,
            'message' => 'Randon interests',
            'data'    => $randonInterests
        ]);
    }

    public function getInterests()
    {
        $userInterestIds = UserInterest::where('user_id', Auth::id())->pluck('interest_id')->toArray();
        $interests = Interest::whereIn('id', $userInterestIds)->get();

        return response()->json([
            'success' => true,
            'message' => 'Interests',
            'data'    => $interests
        ]);
    }

    public function addInterest(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'interest_ids'  => 'required'
        // ]);
        //
        // if ($validator->fails()) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => $validator->errors()
        //     ]);
        // }

        // $interest = Interest::where('id', $request->interest_id)->first();
        // if (!$interest) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'Interest not found!'
        //     ]);
        // }
        //
        // $existInterest = UserInterest::where('user_id', Auth::id())->where('interest_id', $request->interest_id)->first();
        // if ($existInterest) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'This interest is already exists.'
        //     ]);
        // }

        $interestIds = $request->interest_ids;
        UserInterest::where('user_id', Auth::id())->delete();
        foreach ($interestIds as $key => $interestId) {
            // if (!$interest) {
                UserInterest::create([
                    'user_id'     => Auth::id(),
                    'interest_id' => $interestId
                ]);
            // }
        }
        // $interest = UserInterest::create([
        //     'user_id'     => Auth::id(),
        //     'interest_id' => $request->interest_id
        // ]);

        return response()->json([
            'success' => true,
            'message' => 'Interest added successfully.'
        ]);
    }
}
