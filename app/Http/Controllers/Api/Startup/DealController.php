<?php

namespace App\Http\Controllers\Api\Startup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Deal;
use App\Models\BusinessCategory;
use App\Models\BusinessType;
use App\Models\FundingType;
use App\Models\DealLike;
use App\Models\DealComment;
use App\Models\DealProposal;
use App\Models\DealCommentReply;
use App\Models\DealCommentLike;
use Validator;
use Auth;

class DealController extends Controller
{
    public function deals(Request $request)
    {
        $deals = Deal::where('user_id', Auth::id());

        if ($request->business_category_id) {
            $deals = $deals->where('business_category_id', $request->business_category_id);
        }
        if ($request->business_type_id) {
            $deals = $deals->where('business_type_id', $request->business_type_id);
        }
        if ($request->funding_type_id) {
            $deals = $deals->where('funding_type_id', $request->funding_type_id);
        }
        if ($request->funding_amount) {
            $deals = $deals->where('funding_amount', $request->funding_amount);
        }
        if ($request->search) {
            $deals = $deals->where('name', 'like', $request->search.'%');
        }

        $deals = $deals->latest()->get();
        foreach ($deals as $key => $deal) {
            $user             = User::where('id', $deal->user_id)->first();
            $businessCategory = BusinessCategory::where('id', $deal->business_category_id)->first();
            $businessType     = BusinessType::where('id', $deal->business_type_id)->first();
            $fundingType      = FundingType::where('id', $deal->funding_type_id)->first();
            $dealLikes        = DealLike::where('deal_id', $deal->id)->count();
            $dealComments     = DealComment::where('deal_id', $deal->id)->orderBy('id', 'desc')->get();

            $deals[$key]['user']              = $user ? $user->name : '';
            $deals[$key]['profile_image']     = $user ? $user->profile_image : '';
            $deals[$key]['is_private']        = $user ? $user->is_private : ''; // 0 is public and 1 is private
            $deals[$key]['business_category'] = $businessCategory ? $businessCategory->name : '';
            $deals[$key]['business_type']     = $businessType ? $businessType->name : '';
            $deals[$key]['funding_type']      = $fundingType ? $fundingType->name : '';
            $deals[$key]['likes']             = $dealLikes;
            $deals[$key]['comments_count']    = $dealComments->count();

            $deals[$key]['is_liked']          = 0;
            $authLike = DealLike::where('user_id', Auth::id())->where('deal_id', $deal->id)->first();
            if ($authLike) {
                $deals[$key]['is_liked']  = 1;
            }

            // foreach ($dealComments as $commentKey => $dealComment) {
            //     $dealCommentUser = User::where('id', $dealComment->user_id)->first();
            //     $dealComments[$commentKey]['user'] = $dealCommentUser->name;
            // }
            // $deals[$key]['comments'] = $dealComments;
        }

        return response()->json([
            'success' => true,
            'message' => 'Deals list',
            'data'    => $deals
        ]);
    }

    public function dealDetails($dealId)
    {
        $deal = Deal::where('id', $dealId)->where('user_id', Auth::id())->first();
        if (!$deal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal not found'
            ]);
        }

        $user             = User::where('id', $deal->user_id)->first();
        $businessCategory = BusinessCategory::where('id', $deal->business_category_id)->first();
        $businessType     = BusinessType::where('id', $deal->business_type_id)->first();
        $fundingType      = FundingType::where('id', $deal->funding_type_id)->first();
        $dealLikes        = DealLike::where('deal_id', $deal->id)->count();
        $dealComments     = DealComment::where('deal_id', $deal->id)->orderBy('id', 'desc')->get();

        $deal['user']              = $user ? $user->name : '';
        $deal['profile_image']     = $user->profile_image;
        $deal['business_category'] = $businessCategory ? $businessCategory->name : '';
        $deal['business_type']     = $businessType ? $businessType->name : '';
        $deal['funding_type']      = $fundingType ? $fundingType->name : '';
        $deal['likes']             = $dealLikes;
        $deal['comments_count']    = $dealComments->count();

        $deal['is_liked']          = 0;
        $authLike = DealLike::where('user_id', Auth::id())->where('deal_id', $deal->id)->first();
        if ($authLike) {
            $deal['is_liked']  = 1;
        }

        foreach ($dealComments as $key => $dealComment) {
            $dealCommentUser                     = User::where('id', $dealComment->user_id)->first();
            $dealComments[$key]['user']          = $dealCommentUser->name;
            $dealComments[$key]['profile_image'] = $dealCommentUser->profile_image;

            $commentReplyCount = DealCommentReply::where('deal_comment_id', $dealComment->id)->count();
            $dealComments[$key]['comment_reply_count'] = $commentReplyCount;

            $dealCommentLikes = DealCommentLike::where('deal_comment_id', $dealComment->id)->count();
            $dealComments[$key]['like_count'] = $dealCommentLikes;
            $dealComments[$key]['is_liked']   = 0;
            $isLiked = DealCommentLike::where('user_id', Auth::id())->where('deal_comment_id', $dealComment->id)->first();
            if ($isLiked) {
                $dealComments[$key]['is_liked'] = 1;
            }
        }
        $deal['comments'] = $dealComments;

        if ($deal->admin_approval) {
            if ($deal->admin_approval == 1) {
                $deal['deal_status_desc'] = 'Your deal has been approved and it is now available for board members/investors for funding.';
            } else {
                $deal['deal_status_desc'] = $deal->rejection_reason;
            }
        } else {
            $deal['deal_status_desc'] = 'Your deal has been submitted for review. You will be notified once it get’s feedback from authority.';
        }

        return response()->json([
            'success' => true,
            'message' => 'Deal detail.',
            'data'    => $deal
        ]);
    }

    public function addDeal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                 => 'required',
            'business_category_id' => 'required',
            'description'          => 'required',
            'business_type_id'     => 'required',
            'estimate_revenue'     => 'required',
            'estimate_profit'      => 'required',
            'funding_amount'       => 'required',
            'funding_goal'         => 'required',
            'funding_type_id'      => 'required',
            'document'             => 'required',
            'equity'               => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $document = $request->file('document');
        $docName  = $document->getClientOriginalName();
        $fileName = time(). '.' . $document->extension();
        $docPath  = storage_path('app/public') . '/deal-documents/';
        $document->move($docPath, $fileName);

        $deal = Deal::create([
            'user_id'              => Auth::id(),
            'name'                 => $request->name,
            'business_category_id' => $request->business_category_id,
            'description'          => $request->description,
            'business_type_id'     => $request->business_type_id,
            'estimate_revenue'     => $request->estimate_revenue,
            'estimate_profit'      => $request->estimate_profit,
            'funding_amount'       => $request->funding_amount,
            'funding_goal'         => $request->funding_goal,
            'funding_type_id'      => $request->funding_type_id,
            'equity'               => $request->equity,
            'doc_name'             => $docName,
            'doc'                  => '/deal-documents/' . $fileName
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Your deal has been submitted for review. You will be notified once it get’s feedback from authority.',
            'data'    => $deal
        ]);
    }

    public function editDeal(Request $request, $dealId)
    {
        $validator = Validator::make($request->all(), [
            'name'                 => 'required',
            'business_category_id' => 'required',
            'description'          => 'required',
            'business_type_id'     => 'required',
            'estimate_revenue'     => 'required',
            'estimate_profit'      => 'required',
            'funding_amount'       => 'required',
            'funding_goal'         => 'required',
            'funding_type_id'      => 'required',
            'equity'               => 'required',
            'document'             => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $deal = Deal::where('id', $dealId)->first();
        if (!$deal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal not found!'
            ]);
        }

        if ($deal->admin_approval != 2) {
            return response()->json([
                'success' => false,
                'message' => "You can make changes in the deal only after the deal is rejected."
            ]);
        }

        $document = $request->file('document');
        $docName  = $document->getClientOriginalName();
        $fileName = time(). '.' . $document->extension();
        $docPath  = storage_path('app/public') . '/deal-documents/';
        $document->move($docPath, $fileName);

        $oldDoc = str_replace(env('APP_URL').'/storage', '', $deal->doc);
        @unlink(storage_path('app/public'). $oldDoc);

        $deal->update([
            'user_id'              => Auth::id(),
            'name'                 => $request->name,
            'business_category_id' => $request->business_category_id,
            'description'          => $request->description,
            'business_type_id'     => $request->business_type_id,
            'estimate_revenue'     => $request->estimate_revenue,
            'estimate_profit'      => $request->estimate_profit,
            'funding_amount'       => $request->funding_amount,
            'funding_goal'         => $request->funding_goal,
            'funding_type_id'      => $request->funding_type_id,
            'equity'               => $request->equity,
            'doc_name'             => $docName,
            'doc'                  => '/deal-documents/' . $fileName,
            'admin_approval'       => 0,
            'rejection_reason'     => NULL
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Deal updated successfully.',
            'data'    => $deal
        ]);
    }

    public function deleteDeal($dealId)
    {
        $deal = Deal::where('id', $dealId)->first();
        if (!$deal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal not found!',
            ]);
        }

        $deal->delete();

        return response()->json([
            'success' => true,
            'message' => 'Deal deleted successfully.',
        ]);
    }

    public function likeUnlikeDeal($dealId)
    {
        $deal = Deal::where('id', $dealId)->first();
        if (!$deal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal not found'
            ]);
        }

        $dealLike = DealLike::where('deal_id', $dealId)->where('user_id', Auth::id())->first();
        if ($dealLike) {
            $dealLike->delete();
            $likeDeals = DealLike::where('deal_id', $dealId)->get();
            $data['is_liked'] = 0;
            $data['like_count'] = count($likeDeals);

            return response()->json([
                'success' => true,
                'message' => 'Deal unliked successfully.',
                'data'    => $data
            ]);
        }

        $dealLike = DealLike::create([
            'deal_id' => $dealId,
            'user_id' => Auth::id()
        ]);
        $likeDeals = DealLike::where('deal_id', $dealId)->get();
        $data['is_liked'] = 1;
        $data['like_count'] = count($likeDeals);

        return response()->json([
            'success' => true,
            'message' => 'Deal liked successfully.',
            'data'    => $data
        ]);
    }

    public function addDealComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
            'deal_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $deal = Deal::where('id', $request->deal_id)->first();
        if (!$deal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal not found!'
            ]);
        }

        DealComment::create([
            'user_id' => Auth::id(),
            'deal_id' => $request->deal_id,
            'comment' => $request->comment
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Comment added successfully!',
        ]);
    }

    public function getDealproposals($type, $status = null)
    {
        $dealIds = Deal::where('user_id', Auth::id())->pluck('id')->toArray();
        $dealProposals = DealProposal::whereIn('deal_id', $dealIds)->where('user_role', $type);

        if ($status) {
            $dealProposals = $dealProposals->where('status', $status);
        }

        $dealProposals = $dealProposals->get();

        foreach ($dealProposals as $key => $dealProposal) {
            $user = User::where('id', $dealProposal->user_id)->first();
            $deal = Deal::where('id', $dealProposal->deal_id)->first();
            $businessType = BusinessType::where('id', $deal->business_type_id)->first();
            $dealProposals[$key]['user_name']     = $user ? $user->name : '';
            $dealProposals[$key]['user_profile']  = $user ? $user->profile_image : '';
            $dealProposals[$key]['deal_name']     = $deal ? $deal->name : '';
            $dealProposals[$key]['business_type'] = $businessType ? $businessType->name : '';
        }

        return response()->json([
            'success' => true,
            'message' => 'Deal proposals',
            'data'    => $dealProposals
        ]);
    }

    public function getDealproposalsDetails($dealProposalId)
    {
        $dealProposal = DealProposal::where('id', $dealProposalId)->first();
        if (!$dealProposal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal proposal not found!'
            ]);
        }

        $user = User::where('id', $dealProposal->user_id)->first();
        $deal = Deal::where('id', $dealProposal->deal_id)->first();
        $businessType = BusinessType::where('id', $deal->business_type_id)->first();
        $dealProposal['user_name']     = $user ? $user->name : '';
        $dealProposal['user_profile']  = $user ? $user->profile_image : '';
        $dealProposal['deal_name']     = $deal ? $deal->name : '';
        $dealProposal['business_type'] = $businessType ? $businessType->name : '';

        return response()->json([
            'success' => true,
            'message' => 'Deal proposal details.',
            'data'    => $dealProposal
        ]);
    }

    public function acceptRejectProposal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $dealProposal = DealProposal::where('id', $request->deal_proposal_id)->first();
        if (!$dealProposal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal not found!'
            ]);
        }

        $dealProposal->update([
            'status'  => $request->status
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Status updated successfully.'
        ]);
    }
}
