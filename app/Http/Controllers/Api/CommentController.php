<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DealComment;
use App\Models\DealCommentReply;
use App\Models\DealCommentLike;
use App\Models\DealCommentReplyLike;
use App\Models\User;
use Auth;
use Validator;

class CommentController extends Controller
{
    public function addDealCommentReply(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'deal_comment_id' => 'required',
            'reply'           => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        DealCommentReply::create([
            'deal_comment_id' => $request->deal_comment_id,
            'reply_by'        => Auth::id(),
            'reply_to'        => $request->reply_to ?? NULL,
            'reply'           => $request->reply
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Reply added on comment successfully.'
        ]);
    }

    public function getDealCommentReplies($commentId)
    {
        $comment = DealComment::where('id', $commentId)->first();
        if (!$comment) {
            return response()->json([
                'success' => false,
                'message' => 'Comment not found!'
            ]);
        }
        $user = User::where('id', $comment->user_id)->first();
        $comment['user_name'] = $user ? $user->name : '';
        $comment['user_profile'] = $user ? $user->profile_image : '';
        $commentLikes = DealCommentLike::where('deal_comment_id', $comment->id)->count();
        $comment['like_count'] = $commentLikes;
        $comment['is_liked']   = 0;
        $isLiked = DealCommentLike::where('user_id', Auth::id())->where('deal_comment_id', $comment->id)->first();
        if ($isLiked) {
            $comment['is_liked'] = 1;
        }

        $commentReplies = DealCommentReply::where('deal_comment_id', $comment->id)->get();
        foreach ($commentReplies as $key => $commentReplie) {
            $replyByUser = User::where('id', $commentReplie->reply_by)->first();
            $commentReplies[$key]['reply_by_user'] = $replyByUser ? $replyByUser->name : '';
            $commentReplies[$key]['reply_by_user_profile'] = $replyByUser ? $replyByUser->profile_image : '';
            $commentReplies[$key]['reply_by_user_role'] = $replyByUser ? $replyByUser->role : '';
            $commentReplies[$key]['reply_to_user'] = '';
            $commentReplies[$key]['reply_to_user_role'] = '';
            if ($commentReplie->reply_to) {
                $replyToUser = User::where('id', $commentReplie->reply_to)->first();
                $commentReplies[$key]['reply_to_user'] = $replyToUser ? $replyToUser->name : '';
                $commentReplies[$key]['reply_to_user_role'] = $replyToUser ? $replyToUser->role : '';
            }

            $commentReplieLikesCount = DealCommentReplyLike::where('deal_comment_reply_id', $commentReplie->id)->count();
            $commentReplies[$key]['like_count'] = $commentReplieLikesCount;
            $commentReplies[$key]['is_liked']   = 0;
            $isLiked = DealCommentReplyLike::where('user_id', Auth::id())->where('deal_comment_reply_id', $commentReplie->id)->first();
            if ($isLiked) {
                $commentReplies[$key]['is_liked'] = 1;
            }
        }
        $comment['deal_comment_replies'] = $commentReplies;

        return response()->json([
            'success' => true,
            'message' => 'Comment reply list',
            'data'    => $comment
        ]);
    }

    public function likeUnlikeComment($commentId)
    {
        $comment = DealComment::where('id', $commentId)->first();
        if (!$comment) {
            return response()->json([
                'success' => false,
                'message' => 'Comment not found!'
            ]);
        }

        $dealCommentLike = DealCommentLike::where('user_id', Auth::id())->where('deal_comment_id', $commentId)->first();
        if ($dealCommentLike) {
            $dealCommentLike->delete();
            $msg = 'Comment unlike successfully.';
            $data['is_liked'] = 0;
        } else {
            DealCommentLike::create([
                'user_id' => Auth::id(),
                'deal_comment_id' => $commentId
            ]);
            $msg = 'Comment like successfully';
            $data['is_liked'] = 1;
        }
        $likeDeals = DealCommentLike::where('deal_comment_id', $commentId)->count();
        $data['like_count'] = $likeDeals;

        return response()->json([
            'success' => true,
            'message' => $msg,
            'data'    => $data
        ]);
    }

    public function likeUnlikeCommentReply($replyId)
    {
        $reply = DealCommentReply::where('id', $replyId)->first();
        if (!$reply) {
            return response()->json([
                'success' => false,
                'message' => 'Comment reply not found!'
            ]);
        }

        $dealCommentReplyLike = DealCommentReplyLike::where('user_id', Auth::id())->where('deal_comment_reply_id', $replyId)->first();
        if ($dealCommentReplyLike) {
            $dealCommentReplyLike->delete();
            $msg = 'Reply unlike successfully.';
            $data['is_liked'] = 0;
        } else {
            DealCommentReplyLike::create([
                'user_id' => Auth::id(),
                'deal_comment_reply_id' => $replyId
            ]);
            $msg = 'Reply like successfully.';
            $data['is_liked'] = 1;
        }
        $likedReplyCount = DealCommentReplyLike::where('deal_comment_reply_id', $replyId)->count();
        $data['like_count'] = $likedReplyCount;

        return response()->json([
            'success' => true,
            'message' => $msg,
            'data'    => $data
        ]);
    }
}
