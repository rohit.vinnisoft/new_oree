<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MyNetwork;
use App\Models\UserBlock;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Validator;

class MyNetworkController extends Controller
{
    public function sendConnectRequest($connectedUserId)
    {
        $user = User::where('id', $connectedUserId)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found!'
            ]);
        }

        $myNetwork = MyNetwork::where('user_id', Auth::id())->where('connected_user_id', $connectedUserId)->first();
        if (!$myNetwork) {
            MyNetwork::create([
                'user_id'           => Auth::id(),
                'connected_user_id' => $connectedUserId,
            ]);

            $msg = 'Connection request sent successfully.';
            $status = 1;
        } else {
            $otherNetwork = MyNetwork::where('user_id', $myNetwork->connected_user_id)->where('connected_user_id', Auth::id())->first();
            if ($otherNetwork) {
                $otherNetwork->delete();
            }
            $myNetwork->delete();
            $msg = 'Connection request cancelled successfully.';
            $status = 0;
        }

        $userDeviceToken = User::where('id', $connectedUserId)->where('device_token', '!=', NULL)->first();
        if ($userDeviceToken) {
            \Artisan::call('send:notification', [
                'user_id' => $connectedUserId,
                'msg'     => 'Has sent request',
                'deal_id' => null
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => $msg,
            'data'    => ['is_connected' => $status]
        ]);
    }

    public function cancelConnectRequest($connectedUserId)
    {
        $user = User::where('id', $connectedUserId)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found!'
            ]);
        }

        $myNetwork = MyNetwork::where('user_id', Auth::id())->where('connected_user_id', $connectedUserId)->first();
        if (!$myNetwork) {
            return response()->json([
              'success' => false,
              'message' => 'Connection not found!'
            ]);
        }

        $myNetwork->delete();
        return response()->json([
          'success' => true,
          'message' => 'Connection request cancelled successfully.'
        ]);
    }

    public function getConnectedInvestors(Request $request)
    {
        $myNetworkUserIds = MyNetwork::where('user_id', Auth::id())->where('is_connected', 1)->pluck('connected_user_id')->toArray();
        $blockedUserIds   = UserBlock::where('user_id', Auth::id())->pluck('blocked_user_id')->toArray();
        $investorUsers    = User::where('role', 'investor')->whereIn('id', $myNetworkUserIds)->whereNotIn('id', $blockedUserIds);
        if ($request->search) {
            $investorUsers = $investorUsers->where('name', 'like', $request->search."%");
        }
        $investorUsers    = $investorUsers->get();
        $data['investor_count'] = $investorUsers->count();
        $data['investors']      = $investorUsers;

        return response()->json([
            'success' => true,
            'message' => 'Connected investor list.',
            'data'    => $data
        ]);
    }

    public function getConnectedStartups(Request $request)
    {
        $myNetworkUserIds = MyNetwork::where('user_id', Auth::id())->where('is_connected', 1)->pluck('connected_user_id')->toArray();
        $blockedUserIds   = UserBlock::where('user_id', Auth::id())->pluck('blocked_user_id')->toArray();
        $startupUsers     = User::where('role', 'start_up')->whereIn('id', $myNetworkUserIds)->whereNotIn('id', $blockedUserIds);
        if ($request->search) {
            $startupUsers = $startupUsers->where('name', 'like', $request->search."%");
        }
        $startupUsers     = $startupUsers->get();
        $data['startup_count'] = $startupUsers->count();
        $data['startups']      = $startupUsers;

        return response()->json([
            'success' => true,
            'message' => 'Connected startup list.',
            'data'    => $data
        ]);
    }

    public function getInvitations()
    {
        $invitations = MyNetwork::where('connected_user_id', Auth::id())->where('is_connected', 0)->get();
        $authNetworks = MyNetwork::where('user_id', Auth::id())->where('is_connected', 1)->pluck('connected_user_id')->toArray();
        foreach ($invitations as $key => $invitation) {
            $user = User::where('id', $invitation->user_id)->first();
                $invitations[$key]['name'] = $user ? $user->first_name. ' ' .$user->last_name : '';
                $invitations[$key]['profile'] = $user->profile_image ?? '';
                $invitations[$key]['role'] = $user->role ?? '';
                $invitations[$key]['shared_connections'] = 0;
            if ($user) {
                $invationNetworks = MyNetwork::where('user_id', $user->id)->where('is_connected', 1)->pluck('connected_user_id')->toArray();
                $array = array_intersect($authNetworks, $invationNetworks);
                $invitations[$key]['shared_connections'] = count($array);
            }
        }

        return response()->json([
            'success' => true,
            'message' => 'Invitations list.',
            'data'    => $invitations
        ]);
    }

    public function acceptRejectRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'request_id' => 'required',
            'status'     => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $acceptedInvitation = MyNetwork::where('connected_user_id', Auth::id())->where('id', $request->request_id)->where('is_connected', 1)->first();
        if ($acceptedInvitation) {
            return response()->json([
                'success' => false,
                'message' => 'Request is already accepted.'
            ]);
        }

        $invitation = MyNetwork::where('connected_user_id', Auth::id())->where('id', $request->request_id)->where('is_connected', 0)->first();
        if (!$invitation) {
            return response()->json([
                'success' => false,
                'message' => 'Request not found!'
            ]);
        }

        if ($request->status == 'accept') {
            $invitation->update([
                'is_connected'  => 1
            ]);
            $otherInvitation = MyNetwork::where('user_id', Auth::id())->where('connected_user_id', $invitation->user_id)->first();
            if (!$otherInvitation) {
                MyNetwork::create([
                    'user_id' => Auth::id(),
                    'connected_user_id' => $invitation->user_id,
                    'is_connected' => 1,
                ]);
            } else {
                $otherInvitation->update([
                    'is_connected' => 1
                ]);
            }
            $msg = 'Request accepted successfully.';
        } else {
            $invitation->delete();
            $msg = 'Request rejected successfully.';
        }

        return response()->json([
            'success' => true,
            'message' => $msg
        ]);
    }
}
