<?php

namespace App\Http\Controllers\Api\Investor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Deal;
use App\Models\MyNetwork;
use App\Models\UserInterest;
use App\Models\Interest;
use Auth;
use Validator;

class ProfileController extends Controller
{
    public function getProfile()
    {
        $user       = User::where('id', Auth::id())->first();
        $dealsCount = Deal::where('user_id', Auth::id())->count();
        $user['deal_count']  = $dealsCount;
        $user['connections'] = MyNetwork::where('user_id', Auth::id())->where('is_connected', 1)->count();
        $interestIds         = UserInterest::where('user_id', Auth::id())->pluck('interest_id')->toArray();
        $user['interests']   = Interest::whereIn('id', $interestIds)->get();

        return response()->json([
            'success' => true,
            'message' => 'User Profile',
            'data'    => $user
        ]);
    }

    public function editProfile(Request  $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required',
            'description'   => 'required',
            'date_of_birth' => 'required',
            'country'       => 'required',
            'state'         => 'required',
            'city'          => 'required',
            'zip_code'      => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $user = User::where('id', Auth::id())->first();
        $user->update([
            'first_name'    => $request->first_name,
            'last_name'     => $request->last_name,
            'name'          => $request->first_name . ' ' . $request->last_name,
            'email'         => $request->email,
            'description'   => $request->description ?? $user->description,
            'date_of_birth' => $request->date_of_birth,
            'country'       => $request->country,
            'state'         => $request->state,
            'city'          => $request->city,
            'zip_code'      => $request->zip_code,
            'company_name'  => $request->company_name ?? $user->company_name
        ]);

        return response()->json([
            'success' => true,
            'message' => 'User updated successfully.',
            'data'    => $user
        ]);
    }

    public function updateProfileImage(Request $request)
    {
        $user = User::where('id', Auth::id())->first();

        $image = $request->profile_image;
        $imageName = $image->getClientOriginalName();
        $fileName = time() . '.' . $image->extension();
        $filePath = storage_path('app/public') . '/profile-images/';

        $oldImage = str_replace(env('APP_URL').'/storage', '', $user->profile_image);
        @unlink(storage_path('app/public'). $oldImage);

        $image->move($filePath, $fileName);

        $user->update([
            'profile_image' => '/profile-images/'.$fileName
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Profile image updated successfully.',
            'data'    => $user
        ]);
    }

    public function updateCoverImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cover_image' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $user = User::where('id', Auth::id())->first();

        $image = $request->cover_image;
        $imageName = $image->getClientOriginalName();
        $fileName = time() . '.' . $image->extension();
        $filePath = storage_path('app/public') . '/profile-images/';

        $oldImage = str_replace(env('APP_URL').'/storage', '', $user->cover_image);
        @unlink(storage_path('app/public'). $oldImage);

        $image->move($filePath, $fileName);

        $user->update([
            'cover_image' => '/profile-images/'.$fileName
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Cover image updated successfully.',
            'data'    => $user
        ]);
    }

    public function deleteCoverImage(Request $request)
    {
        $user = User::where('id', Auth::id())->first();

        $oldImage = str_replace(env('APP_URL').'/storage', '', $user->cover_image);
        @unlink(storage_path('app/public'). $oldImage);

        $user->update([
            'cover_image' => NULL
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Cover image updated successfully.',
            'data'    => $user
        ]);
    }
}
