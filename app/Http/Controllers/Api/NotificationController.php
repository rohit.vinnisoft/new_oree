<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\User;
use Auth;

class NotificationController extends Controller
{
    public function notifications()
    {
        $notifications = Notification::where('send_to', Auth::id())->orderBy('id', 'DESC')->get();
        foreach ($notifications as $key => $notification) {
            $user = User::where('id', $notification->send_by)->first();
            $notifications[$key]['sender_name'] = $user ? $user->name : '';
            $notifications[$key]['sender_picture'] = $user ? $user->profile_image : '';
        }

        return response()->json([
            'success' => true,
            'message' => 'Notifications list.',
            'data'    => $notifications
        ]);
    }
}
