<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserBlock;
use Auth;
use Validator;

class SettingController extends Controller
{
    public function getBlockedUsers()
    {
        $blockedUsers = UserBlock::where('user_id', Auth::id())->pluck('blocked_user_id')->toArray();
        $users = User::whereIn('id', $blockedUsers)->get();

        return response()->json([
            'success' => true,
            'message' => 'Blocked users list.',
            'data'    => $users
        ]);
    }

    public function blockUnblockUser($userId)
    {
        $user = User::where('id', $userId)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found!'
            ]);
        }

        $userBlock = UserBlock::where('user_id', Auth::id())->where('blocked_user_id', $userId)->first();
        if ($userBlock) {
            $userBlock->delete();

            return response()->json([
                'success' => true,
                'message' => 'User unblocked successfully.',
                'status'  => 0
            ]);
        }

        UserBlock::create([
            'user_id'         => Auth::id(),
            'blocked_user_id' => $userId
        ]);

        return response()->json([
            'success' => true,
            'message' => 'User blocked successfully.',
            'status'  => 1
        ]);
    }

    public function changeAccountStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $user = Auth::user();
        if ($request->status == 'private') {
            $status = 1;
            $msg = "Account changed to private.";
        } else {
            $status = 0;
            $msg = "Account changed to public.";
        }

        $user->update([
            'is_private' => $status
        ]);

        return response()->json([
            'success' => true,
            'message' => $msg,
            'data'    => $user
        ]);
    }
}
