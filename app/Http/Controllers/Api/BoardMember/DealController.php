<?php

namespace App\Http\Controllers\Api\BoardMember;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Deal;
use App\Models\User;
use App\Models\BusinessCategory;
use App\Models\BusinessType;
use App\Models\FundingType;
use App\Models\DealLike;
use App\Models\DealComment;
use App\Models\DealProposal;
use App\Models\UserBlock;
use App\Models\DealsStatus;
use Auth;
use Validator;

class DealController extends Controller
{
    public function getDeals(Request $request)
    {
        $blockedUsersIds = UserBlock::where('user_id', Auth::id())->pluck('blocked_user_id')->toArray();
        $dealIds  = DealsStatus::where('user_id', Auth::id())->pluck('deal_id')->toArray();
        $deals    = Deal::where('status', '!=', 'reject')
                        ->whereNotIn('user_id', $blockedUsersIds)
                        ->whereNotIn('id', $dealIds)
                        ->where('admin_approval', 1);

        if ($request->business_category_id) {
            $deals = $deals->where('business_category_id', $request->business_category_id);
        }
        if ($request->business_type_id) {
            $deals = $deals->where('business_type_id', $request->business_type_id);
        }
        if ($request->funding_type_id) {
            $deals = $deals->where('funding_type_id', $request->funding_type_id);
        }
        if ($request->funding_amount) {
            $deals = $deals->where('funding_amount', $request->funding_amount);
        }
        if ($request->search) {
            $deals = $deals->where('name', 'like', $request->search.'%');
        }

        $deals = $deals->latest()->get();

        foreach ($deals as $key => $deal) {
            $user             = User::where('id', $deal->user_id)->first();
            $businessCategory = BusinessCategory::where('id', $deal->business_category_id)->first();
            $businessType     = BusinessType::where('id', $deal->business_type_id)->first();
            $fundingType      = FundingType::where('id', $deal->funding_type_id)->first();
            $dealLikes        = DealLike::where('deal_id', $deal->id)->count();
            $dealComments     = DealComment::where('deal_id', $deal->id)->orderBy('id', 'desc')->get();

            $deals[$key]['user']              = $user ? $user->name : '';
            $deals[$key]['profile_image']     = $user ? $user->profile_image : '';
            $deals[$key]['is_private']        = $user ? (int) $user->is_private : 0; // 0 is public and 1 is private
            $deals[$key]['business_category'] = $businessCategory ? $businessCategory->name : '';
            $deals[$key]['business_type']     = $businessType ? $businessType->name : '';
            $deals[$key]['funding_type']      = $fundingType ? $fundingType->name : '';
            $deals[$key]['likes']             = $dealLikes;
            $deals[$key]['comments_count']    = $dealComments->count();

            $deals[$key]['is_liked']          = 0;
            $authLike = DealLike::where('user_id', Auth::id())->where('deal_id', $deal->id)->first();
            if ($authLike) {
                $deals[$key]['is_liked']  = 1;
            }

            // foreach ($dealComments as $commentKey => $dealComment) {
            //     $dealCommentUser = User::where('id', $dealComment->user_id)->first();
            //     $dealComments[$commentKey]['user'] = $dealCommentUser->name;
            // }
            // $deals[$key]['comments'] = $dealComments;
        }

        return response()->json([
            'success' => true,
            'message' => 'All deals',
            'data'    => $deals
        ]);
    }

    public function dealDetails($dealId)
    {
        $deal = Deal::where('id', $dealId)->first();
        if (!$deal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal not found'
            ]);
        }

        $user             = User::where('id', $deal->user_id)->first();
        $businessCategory = BusinessCategory::where('id', $deal->business_category_id)->first();
        $businessType     = BusinessType::where('id', $deal->business_type_id)->first();
        $fundingType      = FundingType::where('id', $deal->funding_type_id)->first();
        $dealLikes        = DealLike::where('deal_id', $deal->id)->count();
        $dealComments     = DealComment::where('deal_id', $deal->id)->orderBy('id', 'desc')->get();

        $deal['user']              = $user ? $user->name : '';
        $deal['profile_image']     = $user ? $user->profile_image : '';
        $deal['business_category'] = $businessCategory ? $businessCategory->name : '';
        $deal['business_type']     = $businessType ? $businessType->name : '';
        $deal['funding_type']      = $fundingType ? $fundingType->name : '';
        $deal['likes']             = $dealLikes;
        $deal['comments_count']    = $dealComments->count();

        $deal['is_liked']          = 0;
        $authLike = DealLike::where('user_id', Auth::id())->where('deal_id', $deal->id)->first();
        if ($authLike) {
            $deal['is_liked']  = 1;
        }

        foreach ($dealComments as $key => $dealComment) {
            $dealCommentUser                     = User::where('id', $dealComment->user_id)->first();
            $dealComments[$key]['user']          = $dealCommentUser->name;
            $dealComments[$key]['profile_image'] = $dealCommentUser->profile_image;
        }
        $deal['comments'] = $dealComments;

        return response()->json([
            'success' => true,
            'message' => 'Deals list',
            'data'    => $deal
        ]);
    }

    public function sendDealProposal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'deal_id'     => 'required',
            'bid_amount'  => 'required',
            'equity'      => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $deal = Deal::where('id', $request->deal_id)->first();
        if (!$deal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal not found'
            ]);
        }

        // $authDeal = Deal::where('id', $request->deal_id)->where('user_id', Auth::id())->first();
        // if ($authDeal) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'Not a valid deal.'
        //     ]);
        // }

        $dealProposal = DealProposal::where('user_id', Auth::id())->where('deal_id', $request->deal_id)->first();
        if ($dealProposal) {
            $dealProposal->update([
                'bid_amount' => $request->bid_amount,
                'equity'     => $request->equity
            ]);
        } else {
            $dealProposal = DealProposal::create([
                'user_id'    => Auth::id(),
                'user_role'  => Auth::user()->role,
                'deal_id'    => $request->deal_id,
                'bid_amount' => $request->bid_amount,
                'equity'     => $request->equity
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Offer sent successfully.',
            'data'    => $dealProposal
        ]);
    }

    public function myDeals()
    {
        $dealIds  = DealsStatus::where('user_id', Auth::id())->where('status', 0)->pluck('deal_id')->toArray();
        $deals    = Deal::where('status', '!=', 'reject')
                        ->whereIn('id', $dealIds)
                        ->where('admin_approval', 1)->latest()->get();

        foreach ($deals as $key => $deal) {
            $user         = User::where('id', $deal->user_id)->first();
            $businessType = BusinessType::where('id', $deal->business_type_id)->first();

            $deals[$key]['user_name']     = $user ? $user->name : '';
            $deals[$key]['profile_image'] = $user ? $user->profile_image : '';
            $deals[$key]['business_type'] = $businessType ? $businessType->name : '';
        }

        return response()->json([
            'success' => true,
            'message' => 'My deals',
            'data'    => $deals
        ]);

        /* -- Old code -- */
        // $dealProposals = DealProposal::where('user_id', Auth::id())->latest()->get();
        // foreach ($dealProposals as $key => $dealProposal) {
        //     $deal         = Deal::where('id', $dealProposal->deal_id)->first();
        //     $user         = User::where('id', $deal->user_id)->first();
        //     $businessType = BusinessType::where('id', $deal->business_type_id)->first();
        //
        //     $dealProposals[$key]['deal_name']     = $deal->name;
        //     $dealProposals[$key]['user_name']     = $user ? $user->name : '';
        //     $dealProposals[$key]['profile_image'] = $user ? $user->profile_image : '';
        //     $dealProposals[$key]['business_type'] = $businessType ? $businessType->name : '';
        // }
        //
        // return response()->json([
        //     'success' => true,
        //     'message' => 'My deals',
        //     'data'    => $dealProposals
        // ]);
    }

    public function acceptRejectDeal(Request $request) // BoardMember mask deal as interested or not interested
    {
        $validator = Validator::make($request->all(), [
            'deal_id' => 'required',
            'status'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $deal = Deal::where('id', $request->deal_id)->first();
        if (!$deal) {
            return response()->json([
                'success' => false,
                'message' => 'Deal not found!'
            ]);
        }

        DealsStatus::create([
            'deal_id' => $deal->id,
            'user_id' => Auth::id(),
            'status'  => ($request->status == 'interested') ? 0 : 1 // 0 = Interested and 1 = Not Interested
        ]);


        $boardMembers = User::where('is_board_member', 1)
                            // ->where('boardmember_at', '<', $deal->created_at)
                            ->count();
        $status = $deal->status;
        if ($request->status == 'interested') {
            if ($boardMembers == ($deal->interest_count + 1)) {
                $status = 'accept';
            }
            $deal->update([
                'interest_count' => $deal->interest_count + 1,
                'status'         => $status
            ]);
            $msg = 'Deal accepted successfully.';
        } else {
            if ($boardMembers == ($deal->not_interested_count + 1)) {
                $status = 'reject';
            }
            $deal->update([
                'not_interested_count' => $deal->not_interested_count + 1,
                'status'               => $status
            ]);
            $msg = 'Deal rejected successfully.';
        }


        return response()->json([
            'success' => true,
            'message' => $msg
        ]);
    }
}
