<?php

namespace App\Http\Controllers\Api\BoardMember;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Interest;
use App\Models\UserInterest;
use Auth;

class UserController extends Controller
{
    public function getBoardMembers()
    {
        $boradMembers = User::where('id', '!=', Auth::id())->where('role', 'board_member')->get();

        return response()->json([
            'success' => true,
            'message' => 'Board Members list',
            'data'    => $boradMembers
        ]);
    }

    public function getBoardMemberDetails($boardMemberId)
    {
        $boardMember = User::with('boardMembers')->where('id', $boardMemberId)->first();
        $userInterestIds = UserInterest::where('user_id', $investorId)->pluck('interest_id')->toArray();
        $boardMember['interests'] = Interest::whereIn('id', $userInterestIds)->get();

        return response()->json([
            'success' => true,
            'message' => 'Board Member detail',
            'data'    => $boardMember
        ]);
    }
}
