<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Validator;

class UserController extends Controller
{
    public function addUserDeviceToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $user = User::where('id', Auth::id())->first();
        $user->update([
            'device_token' => $request->device_token
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Device token updated successfully.',
            'data'    => $user
        ]);
    }
}
