<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Card;

class CardController extends Controller
{
    public function addCard(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'number'    => 'required',
            'exp_month' => 'required',
            'exp_year'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $existCard = Card::where('number', $request->number)->first();
        if ($existCard) {
            return response()->json([
                'success' => false,
                'message' => 'Card already exist'
            ]);
        }

        $card = Card::create([
            'name'      => $request->name,
            'number'    => $request->number,
            'exp_month' => $request->exp_month,
            'exp_year'  => $request->exp_year
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Card added successfully.',
            'data'    => $card
        ]);
    }
}
