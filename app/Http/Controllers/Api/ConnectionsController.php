<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\MyNetwork;
use App\Models\Deal;
use App\Models\UserBlock;
use App\Models\UserInterest;
use App\Models\Interest;
use Auth;

class ConnectionsController extends Controller
{
    public function getInvestors(Request $request)
    {
        $blockedUserIds = UserBlock::where('user_id', Auth::id())->pluck('blocked_user_id')->toArray();
        $investors = User::where('id', '!=', Auth::id())->whereNotIn('id', $blockedUserIds)->where('role', 'investor');
        if ($request->search) {
            $investors = $investors->where('name', 'like', $request->search.'%');
        }
        $investors = $investors->get();
        foreach ($investors as $key => $investor) {
            $connection = MyNetwork::where('user_id', Auth::id())->where('connected_user_id', $investor->id)->first();
            if (!$connection) {
                $investors[$key]['is_connected'] = 0; // Connection is not created.
            } else {
                if (!$connection->is_connected) {
                    $investors[$key]['is_connected'] = 1; // Connection is created but not accepted.
                } else {
                    $investors[$key]['is_connected'] = 2; // Connection is created and accepted.
                }
            }
        }

        return response()->json([
            'success' => true,
            'message' => 'Investors list',
            'data'    => $investors
        ]);
    }

    public function getStartUps(Request $request)
    {
        $blockedUserIds = UserBlock::where('user_id', Auth::id())->pluck('blocked_user_id')->toArray();
        $startups = User::where('id', '!=', Auth::id())->whereNotIn('id', $blockedUserIds)->where('role', 'start_up');
        if ($request->search) {
            $startups = $startups->where('name', 'like', $request->search.'%');
        }
        $startups = $startups->get();
        foreach ($startups as $key => $startup) {
            $connection = MyNetwork::where('user_id', Auth::id())->where('connected_user_id', $startup->id)->first();
            if (!$connection) {
                $startups[$key]['is_connected'] = 0; // Connection is not created.
            } else {
                if (!$connection->is_connected) {
                    $startups[$key]['is_connected'] = 1; // Connection is created but not accepted.
                } else {
                    $startups[$key]['is_connected'] = 2; // Connection is created and accepted.
                }
            }
        }

        return response()->json([
            'success' => true,
            'message' => 'Startups list',
            'data'    => $startups
        ]);
    }

    public function getInvertorDetails($investorId)
    {
        $investor   = User::where('id', $investorId)->first();
        if (!$investor) {
            return response()->json([
                'success' => false,
                'message' => 'Invertor not found!',
            ]);
        }

        $connection = MyNetwork::where('user_id', Auth::id())->where('connected_user_id', $investor->id)->first();
        if (!$connection) {
            $investor['is_connected'] = 0; // Connection is not created.
        } else {
            if (!$connection->is_connected) {
                $investor['is_connected'] = 1; // Connection is created but not accepted.
            } else {
                $investor['is_connected'] = 2; // Connection is created and accepted.
            }
        }

        $investor['deal_count']  = Deal::where('user_id', $investor->id)->count();
        $investor['connections'] = MyNetwork::where('user_id', $investor->id)->where('is_connected', 1)->count();
        $userBlock = UserBlock::where('user_id', Auth::id())->where('blocked_user_id', $investor->id)->first();
        $userInterestIds = UserInterest::where('user_id', $investorId)->pluck('interest_id')->toArray();
        $investor['interests'] = Interest::whereIn('id', $userInterestIds)->get();
        $investor['is_blocked'] = 0;
        if ($userBlock) {
            $investor['is_blocked'] = 1;
        }

        return response()->json([
            'success' => true,
            'message' => 'Invertor Details',
            'data'    => $investor
        ]);
    }

    public function getStartupDetails($startupId)
    {
        $startup   = User::where('id', $startupId)->first();
        if (!$startup) {
            return response()->json([
                'success' => false,
                'message' => 'Startup not found!',
            ]);
        }

        $connection = MyNetwork::where('user_id', Auth::id())->where('connected_user_id', $startup->id)->first();
        if (!$connection) {
            $startup['is_connected'] = 0; // Connection is not created.
        } else {
            if (!$connection->is_connected) {
                $startup['is_connected'] = 1; // Connection is created but not accepted.
            } else {
                $startup['is_connected'] = 2; // Connection is created and accepted.
            }
        }

        $startup['deal_count']  = Deal::where('user_id', $startup->id)->count();
        $startup['connections'] = MyNetwork::where('user_id', $startup->id)->where('is_connected', 1)->count();
        $userBlock = UserBlock::where('user_id', Auth::id())->where('blocked_user_id', $startup->id)->first();
        $startup['is_blocked'] = 0;
        if ($userBlock) {
            $startup['is_blocked'] = 1;
        }

        return response()->json([
            'success' => true,
            'message' => 'Startup Details',
            'data'    => $startup
        ]);
    }
}
