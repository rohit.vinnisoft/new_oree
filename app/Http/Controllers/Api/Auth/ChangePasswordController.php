<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Hash;
use Auth;

class ChangePasswordController extends Controller
{
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password'  => 'required',
            'new_password'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        if (Hash::check($request->old_password, Auth::user()->password)) {
            $user = Auth::user();
            $user->update([
                'password'  => Hash::make($request->new_password)
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Password changed successfully.',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Old password is incorrect.',
            ]);
        }
    }
}
