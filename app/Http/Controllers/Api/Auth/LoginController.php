<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UsersMembershipPlan;
use Validator;
use Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $existUser = User::where('email', $request->email)->where('login_type', 'manual')->first();
        if ($existUser) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user();
                $user->token = $user->createToken('MyApp')->plainTextToken;
                $usersMembershipPlan = UsersMembershipPlan::where('user_id', Auth::id())->where('expired_at', NULL)->first();
                if ($usersMembershipPlan) {
                    $user->membership_id   = $usersMembershipPlan->membership_id;
                    $user->membership_type = $usersMembershipPlan->membership_type;
                }

                return response()->json([
                    'success' => true,
                    'message' => 'User login successfully.',
                    'data'    => $user
                ]);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Either email or password is incorrect.'
        ]);
    }

    public function checkUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found'
            ]);
        }

        Auth::login($user);
        $user->token = $user->createToken('MyApp')->plainTextToken;

        return response()->json([
            'success' => true,
            'message' => 'User login successfully.',
            'data'    => $user
        ]);
    }

    public function logout(Request $request)
    {
        $token = $request->user()->tokens()->delete();

        return response()->json([
        'success' => true,
        'message' => 'Logout Successfully'
        ]);
    }
}
