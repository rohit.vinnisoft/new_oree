<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Mail\ForgotPassword;
use Validator;
use Mail;

class ForgotPasswordController extends Controller
{
    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $existUser = User::where('email', $request->email)->where('login_type', 'manual')->first();
        if (!$existUser) {
            return response()->json([
                'success' => false,
                'message' => "We can't find a user with that email address."
            ]);
        }

        try {
            $token = encrypt($request->email);
            Mail::to($request->email)->send(new ForgotPassword($token));

            return response()->json([
                'success' => true,
                'message' => 'We have emailed your password reset link!',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Oops, something went wrong.',
            ]);
        }
    }
}
