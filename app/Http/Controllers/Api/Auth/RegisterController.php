<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Investor;
use App\Models\InvestorDocument;
use App\Models\QuizAns;
use App\Models\BoardMember;
use App\Models\BoardMemberDocument;
use Validator;
use Hash;
use Auth;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role'                      => 'required',
            'first_name'                => 'required',
            'last_name'                 => 'required',
            'email'                     => 'required',
            'date_of_birth'             => 'required',
            'country'                   => 'required',
            'state'                     => 'required',
            'city'                      => 'required',
            'zip_code'                  => 'required',
            'password'                  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $existUser = User::where('email', $request->email)->where('login_type', 'manual')->first();
        if ($existUser) {
            return response()->json([
                'success' => false,
                'message' => 'This email is already registered. Please login.'
            ]);
        }

        $isApproved = 1;
        if (($request->role == 'board_member') || ($request->role == 'investor' && ($request->qualified_investor || $request->is_board_member))) {
            $isApproved = 0;
        }

        try {
            $user = User::create([
                'first_name'      => $request->first_name,
                'last_name'       => $request->last_name,
                'name'            => $request->first_name . ' ' . $request->last_name,
                'email'           => $request->email,
                'password'        => Hash::make($request->password),
                'role'            => $request->role,
                'company_name'    => $request->company_name ?? NULL,
                'is_approved'     => $isApproved,
                'date_of_birth'   => $request->date_of_birth,
                'country'         => $request->country,
                'state'           => $request->state,
                'city'            => $request->city,
                'zip_code'        => $request->zip_code,
                'is_board_member' => $request->is_board_member ?? 0,
                'is_qualified_investor' => $request->qualified_investor ?? 0
            ]);

            if ($request->role == "investor") {
                $investor = Investor::create([
                    'user_id'                   => $user->id,
                    'business_category_id'      => $request->business_category_id,
                    'business_type_id'          => $request->business_type_id,
                    'funding_type_id'           => $request->funding_type_id,
                    'preferred_valuation_range' => $request->preferred_valuation_range,
                    'qualified_investor'        => $request->qualified_investor ?? 0
                ]);

                $quidIds = explode(",", $request->quiz_id);
                $quidAns = explode(",", $request->quiz_ans);
                foreach ($quidIds as $key => $quizId) {
                    $quizAns = QuizAns::create([
                        'user_id' => $user->id,
                        'quiz_id' => $quizId,
                        'ans'     => $quidAns[$key],
                    ]);
                }
            }

            if ($request->role == "board_member" || $request->is_board_member) {
                $boardMember = BoardMember::create([
                    'user_id'         => $user->id,
                    'portfolio_id'    => $request->portfolio_id,
                    'industry_id'     => $request->industry_id,
                    'specialty_id'    => $request->specialty_id,
                    'goal'            => $request->goal,
                    'biggest_failure' => $request->biggest_failure,
                    'biggest_professional_success' => $request->biggest_professional_success
                ]);

                // $files = $request->file('files');
                // foreach ($files as $key => $file) {
                //     $docName  = $file->getClientOriginalName();
                //     $fileName = time() . $key . '.' . $file->extension();
                //     $filePath = storage_path('app/public') . '/board-member-documents/';
                //     $file->move($filePath, $fileName);
                //
                //     $boardMemberDocuments = BoardMemberDocument::create([
                //         'user_id'   => $user->id,
                //         'file_name' => $docName,
                //         'file_path' => '/board-member-documents/'.$fileName,
                //     ]);
                // }
            }

            if ($request->qualified_investor || $request->is_board_member) {
                $files = $request->file('files');
                foreach ($files as $key => $file) {
                    $docName  = $file->getClientOriginalName();
                    $fileName = time() . $key . '.' . $file->extension();
                    $filePath = storage_path('app/public') . '/investor-documents/';
                    $file->move($filePath, $fileName);

                    $investorDocuments = InvestorDocument::create([
                        'user_id'   => $user->id,
                        'file_name' => $docName,
                        'file_path' => '/investor-documents/'.$fileName,
                    ]);
                }
            }

            // if (($request->role == 'start_up') || ($request->role == 'investor' && !$request->qualified_investor && !$request->is_board_member)) {
                Auth::login($user);
                $user->token = $user->createToken('MyApp')->plainTextToken;
            // }

            return response()->json([
                'success' => true,
                'message' => 'User register successfully.',
                'data'    => $user
            ]);
        } catch (\Exception $e) {
            $delUser = User::where('email', $request->email)->first();
            Investor::where('user_id', $delUser->id)->delete();
            QuizAns::where('user_id', $delUser->id)->delete();
            InvestorDocument::where('user_id', $delUser->id)->delete();
            BoardMember::where('user_id', $delUser->id)->delete();
            BoardMemberDocument::where('user_id', $delUser->id)->delete();
            $delUser->delete();
            return response()->json([
                'success' => false,
                'message' => 'Oops, something went wrong.',
            ]);
        }
    }
}
