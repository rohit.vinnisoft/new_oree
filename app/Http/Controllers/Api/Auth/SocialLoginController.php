<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Hash;
use Auth;

class SocialLoginController extends Controller
{
    public function socialLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'            => 'required',
            'email'           => 'required',
            'social_id'       => 'required',
            'social_provider' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $user = User::where('social_provider', $request->social_provider)
                    ->where('social_id', $request->social_id)
                    ->first();

        if ($user) {
            $user->update([
                'name'            => $request->name,
                'email'           => $request->email,
                'password'        => Hash::make($request->social_id),
                'social_id'       => $request->social_id,
                'social_provider' => $request->social_provider,
                'profile_image'   => $request->profile_image ?? NULL,
                'login_type'      => $request->social_provider,
                'deviceType'      => $request->device_type ?? '',
                'longitude'       => $request->longitude ?? NULL,
                'latitude'        => $request->latitude ?? NULL,
            ]);
        } else {
            $user = new User;
            $user = $user->create([
                'name'            => $request->name,
                'email'           => $request->email,
                'password'        => Hash::make($request->social_id),
                'social_id'       => $request->social_id,
                'social_provider' => $request->social_provider,
                'profile_image'   => $request->profile_image ?? NULL,
                'login_type'      => $request->social_provider,
                'deviceType'      => $request->device_type ?? '',
                'longitude'       => $request->longitude ?? NULL,
                'latitude'        => $request->latitude ?? NULL,
            ]);
        }

        Auth::login($user, true);
        $user->token = $user->createToken('MyApp')->plainTextToken;

        return response()->json([
          'success' => true,
          'message' => 'User login successfully.',
          'data'    => $user
        ]);
    }
}
