<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Deal;
use App\Models\BusinessCategory;
use App\Models\BusinessType;
use App\Models\FundingType;
use App\Models\DealLike;
use App\Models\MyNetwork;
use App\Models\RecentSearch;
use App\Models\UserBlock;
use Auth;
use Validator;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $user         = Auth::user();
        $data         = [];
        $deals        = [];

        $authNetworks = MyNetwork::where('user_id', Auth::id())->where('is_connected', 1)->pluck('connected_user_id')->toArray();

        $blockedUserIds = UserBlock::where('user_id', Auth::id())->pluck('blocked_user_id')->toArray();
        if (in_array($user->role, ['investor', 'start_up'])) {
            $investors    = [];
            $startups     = [];

            if ($request->type == 'investors') {
                $investors = User::where('id', '!=', Auth::id())->whereNotIn('id', $blockedUserIds)->where('role', 'investor')->where('name', 'like', $request->search.'%')->get();
            } elseif ($request->type == 'startups') {
                $startups  = User::where('id', '!=', Auth::id())->whereNotIn('id', $blockedUserIds)->where('role', 'start_up')->where('name', 'like', $request->search.'%')->get();
            } elseif ($request->type == 'deals') {
                $deals = Deal::where('name', 'like', $request->search.'%');
                if ($user->role == 'start_up') {
                    $deals = $deals->where('user_id', Auth::id())->where('status', '!=', 'reject');
                } else {
                    $deals = $deals->where('status', 'other');
                }
                $deals = $deals->get();
            } else {
                $investors = User::where('id', '!=', Auth::id())->whereNotIn('id', $blockedUserIds)->where('role', 'investor')->where('name', 'like', $request->search.'%')->get();
                $startups  = User::where('id', '!=', Auth::id())->whereNotIn('id', $blockedUserIds)->where('role', 'start_up')->where('name', 'like', $request->search.'%')->get();

                $deals = Deal::where('name', 'like', $request->search.'%');
                if ($user->role == 'start_up') {
                    $deals = $deals->where('user_id', Auth::id())->where('status', '!=', 'reject');
                } else {
                    $deals = $deals->where('status', 'other');
                }
                $deals = $deals->get();
            }

            foreach ($investors as $investorKey => $investor) {
                $investorNetworks = MyNetwork::where('user_id', $investor->id)->where('is_connected', 1)->pluck('connected_user_id')->toArray();
                $array = array_intersect($authNetworks, $investorNetworks);
                $investors[$investorKey]['shared_connections'] = count($array);

                $connectedUsers = User::whereIn('id', $array)->take(3)->pluck('profile_image')->toArray();
                $investors[$investorKey]['shared_connections_profile'] = $connectedUsers;

                $connection = MyNetwork::where('user_id', Auth::id())->where('connected_user_id', $investor->id)->first();
                if (!$connection) {
                    $investors[$investorKey]['is_connected'] = 0; // Connection is not created.
                } else {
                    if (!$connection->is_connected) {
                        $investors[$investorKey]['is_connected'] = 1; // Connection is created but not accepted.
                    } else {
                        $investors[$investorKey]['is_connected'] = 2; // Connection is created and accepted.
                    }
                }
            }
            $data['investors'] = $investors;

            foreach ($startups as $startupKey => $startup) {
                $startupNetworks = MyNetwork::where('user_id', $startup->id)->where('is_connected', 1)->pluck('connected_user_id')->toArray();
                $array = array_intersect($authNetworks, $startupNetworks);
                $startups[$startupKey]['shared_connections'] = count($array);

                $connectedUsers = User::whereIn('id', $array)->take(3)->pluck('profile_image')->toArray();
                $startups[$startupKey]['shared_connections_profile'] = $connectedUsers;

                $connection = MyNetwork::where('user_id', Auth::id())->where('connected_user_id', $startup->id)->first();
                if (!$connection) {
                    $startups[$startupKey]['is_connected'] = 0; // Connection is not created.
                } else {
                    if (!$connection->is_connected) {
                        $startups[$startupKey]['is_connected'] = 1; // Connection is created but not accepted.
                    } else {
                        $startups[$startupKey]['is_connected'] = 2; // Connection is created and accepted.
                    }
                }
            }
            $data['startups'] = $startups;
        }

        if ($user->role == 'board_member') {
            $boardMembers = [];

            if ($request->type == 'board_members') {
                $boardMembers = User::where('role', 'board_member')->where('name', 'like', $request->search.'%')->get();
            } elseif ($request->type == 'deals') {
                $deals = Deal::where('name', 'like', $request->search.'%')->where('status', '!=', 'reject')->get();
            } else {
                $boardMembers = User::where('role', 'board_member')->where('name', 'like', $request->search.'%')->get();
                $deals = Deal::where('name', 'like', $request->search.'%')->where('status', '!=', 'reject')->get();
            }

            foreach ($boardMembers as $boardMemberKey => $boardMember) {
                $boardMemberNetworks = MyNetwork::where('user_id', $boardMember->id)->where('is_connected', 1)->pluck('connected_user_id')->toArray();
                $array = array_intersect($authNetworks, $boardMemberNetworks);
                $boardMembers[$boardMemberKey]['shared_connections'] = count($array);

                $connectedUsers = User::whereIn('id', $array)->take(3)->pluck('profile_image')->toArray();
                $boardMembers[$boardMemberKey]['shared_connections_profile'] = $connectedUsers;
            }
            $data['board_members'] = $boardMembers;
        }

        foreach ($deals as $key => $deal) {
            $user             = User::where('id', $deal->user_id)->first();
            $businessCategory = BusinessCategory::where('id', $deal->business_category_id)->first();
            $businessType     = BusinessType::where('id', $deal->business_type_id)->first();
            $fundingType      = FundingType::where('id', $deal->funding_type_id)->first();

            $deals[$key]['user']              = $user ? $user->name : '';
            $deals[$key]['profile_image']     = $user ? $user->profile_image : '';
            $deals[$key]['is_private']        = $user ? $user->is_private : 0; // 0 is public and 1 is private
            $deals[$key]['business_category'] = $businessCategory ? $businessCategory->name : '';
            $deals[$key]['business_type']     = $businessType ? $businessType->name : '';
            $deals[$key]['funding_type']      = $fundingType ? $fundingType->name : '';
        }
        $data['deals'] = $deals;

        return response()->json([
            'success' => true,
            'message' => 'Search result.',
            'data'    => $data
        ]);
    }

    public function addRecentSearch($userId)
    {
        $user = User::where('id', $userId)->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found!'
            ]);
        }

        $recentSearch = [];
        $existRecentSearch = RecentSearch::where('user_id', Auth::id())->where('search_user_id', $userId)->first();
        if ($existRecentSearch) {
            $existRecentSearch->delete();
        }

        $recentSearch = RecentSearch::create([
            'user_id'        => Auth::id(),
            'search_user_id' => $userId,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Recent search added successfully.',
            'data'    => $recentSearch
        ]);
    }

    public function recentSearches()
    {
        $blockedUserIds = UserBlock::where('user_id', Auth::id())->pluck('blocked_user_id')->toArray();
        $userIds = User::whereNotIn('id', $blockedUserIds)->pluck('id')->toArray();
        $recentSearchs = RecentSearch::where('user_id', Auth::id())->whereIn('search_user_id', $userIds)->orderBy('id', 'DESC')->take(15)->get();
        foreach ($recentSearchs as $key => $recentSearch) {
            $user = User::where('id', $recentSearch->search_user_id)->first();
            $recentSearchs[$key]['user_name'] = $user->name;
            $recentSearchs[$key]['user_profile'] = $user->profile_image;
            $recentSearchs[$key]['user_role'] = $user->role;
        }

        return response()->json([
            'success' => true,
            'message' => 'Recent searchs.',
            'data'    => $recentSearchs
        ]);
    }
}
