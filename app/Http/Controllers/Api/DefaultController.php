<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BusinessCategory;
use App\Models\BusinessType;
use App\Models\FundingType;
use App\Models\Quiz;
use App\Models\Portfolio;
use App\Models\Industry;
use App\Models\Specialty;
use App\Models\Membership;
use App\Models\User;
use App\Models\InvestorBenefit;
use App\Models\BoardMemberBenefit;
use App\Models\UsersMembershipPlan;
use Validator;
use Auth;

class DefaultController extends Controller
{
    public function getCategoryAndType()
    {
        $data['business_category'] = BusinessCategory::all();
        $data['business_type']     = BusinessType::all();
        $data['funding_type']      = fundingType::all();

        return response()->json([
            'success' => true,
            'message' => 'Categories and Types lists',
            'data'    => $data
        ]);
    }

    public function getQuiz()
    {
        $quizzes = Quiz::all();

        return response()->json([
            'success' => true,
            'message' => 'Quiz list',
            'data'    => $quizzes
        ]);
    }

    public function getPortfolioAndOtherData()
    {
        $data['portfolio'] = Portfolio::all();
        $data['industry']  = Industry::all();
        $data['specialty'] = Specialty::all();

        return response()->json([
            'success' => true,
            'message' => 'Portfolio and Other Data lists',
            'data'    => $data
        ]);
    }

    public function getMembershipPlans()
    {
        $membershipPlans = Membership::all();
        foreach ($membershipPlans as $key => $membershipPlan) {
            $monthlyRate = $membershipPlan->monthly_rate;
            $yearlyRate  = $membershipPlan->yearly_rate;
            $save = ($monthlyRate * 12) - $yearlyRate;
            if ($save <= 0) {
                $save = 0;
            }
            $membershipPlans[$key]['save'] = (string) $save;
        }

        return response()->json([
            'success' => true,
            'message' => 'Membership plans list',
            'data'    => $membershipPlans
        ]);
    }

    public function getMembershipPlansBenifits($planId)
    {
        $membershipPlan = Membership::where('id', $planId)->first();
        if (!$membershipPlan) {
            return response()->json([
                'success' => false,
                'message' => 'Membership plan not found',
            ]);
        }
        $membershipPlan['benifits'] = json_decode($membershipPlan->benifits);
        $monthlyRate = $membershipPlan->monthly_rate;
        $yearlyRate  = $membershipPlan->yearly_rate;
        $save = ($monthlyRate * 12) - $yearlyRate;
        if ($save <= 0) {
            $save = 0;
        }
        $membershipPlan['save'] = (string) $save;

        return response()->json([
            'success' => true,
            'message' => 'Membership plan benifits',
            'data'    => $membershipPlan
        ]);
    }

    public function addMembershipPlan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'membership_id'   => 'required',
            'membership_type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $membershipPlan = Membership::where('id', $request->membership_id)->first();
        if (!$membershipPlan && $request->membership_id != 0) {
            return response()->json([
                'success' => false,
                'message' => 'Membership Plan does not exist.'
            ]);
        }

        if ($request->membership_type == 'free') {
            $expiredPlan = UsersMembershipPlan::where('user_id', Auth::id())->where('membership_type', 'free')->whereNotNull('expired_at')->first();
            if ($expiredPlan) {
                return response()->json([
                    'success' => false,
                    'message' => 'You have already used the free trail service.'
                ]);
            }
        }

        $usersMembershipPlan = UsersMembershipPlan::where('user_id', Auth::id())->where('expired_at', NULL)->first();
        if ($usersMembershipPlan) {
            return response()->json([
                'success' => false,
                'message' => 'Membership plan already added.'
            ]);
        } else {
            UsersMembershipPlan::create([
                'user_id'         => Auth::id(),
                'membership_id'   => $request->membership_id ?? 0,
                'membership_type' => $request->membership_type
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Membership plan added successfully.'
            ]);
        }
    }

    public function getInvestorBenifits()
    {
        $investorBenefits = InvestorBenefit::all();

        return response()->json([
            'success' => true,
            'message' => 'Investor benifits list.',
            'data'    => $investorBenefits
        ]);
    }

    public function getBoardMemberBenifits()
    {
        $boardMemberBenefits = BoardMemberBenefit::all();

        return response()->json([
            'success' => true,
            'message' => 'Board Member benifits list.',
            'data'    => $boardMemberBenefits
        ]);
    }
}
