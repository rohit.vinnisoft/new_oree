<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class ChangePasswordController extends Controller
{
    public function changePassword($token)
    {
        $email = decrypt($token);

        return view('change-password', compact('email'));
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'password'         => 'required|min:8',
            'confirm_password' => 'required|same:password'
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return back()->with('error', 'This password reset token is invalid.');
        }

        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return back()->with('status', 'Password updated successfully.');
    }
}
