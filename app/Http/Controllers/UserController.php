<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\BusinessCategory;
use App\Models\BusinessType;
use App\Models\FundingType;
use App\Models\Investor;
use App\Models\BoardMember;
use App\Models\InvestorDocument;
use Hash;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($role)
    {
        $users = User::where('id', '!=', Auth::id());
        // if ($role == 'investor') {
        //     $users = $users->where('is_board_member', 0);
        // } elseif ($role == 'board_member') {
        //     $role = 'investor';
        //     $users = $users->where('is_board_member', 1);
        // }
        $users = $users->where('role', $role)->get();

        return view('admin.users.index', compact('users', 'role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $businessCategories = BusinessCategory::all();
        $businessTypes = BusinessType::all();
        $fundingTypes = FundingType::all();

        return view('admin.users.create', compact('businessCategories', 'businessTypes', 'fundingTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name'                => 'required',
            'last_name'                 => 'required',
            'email'                     => 'required',
            'password'                  => 'required',
            'role'                      => 'required',
            'business_category'         => 'required',
            'business_type'             => 'required',
            'funding_type'              => 'required',
            'preferred_valuation_range' => 'required',
            'files'                     => 'required',
        ]);

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'name'       => $request->first_name . ' ' . $request->last_name,
            'email'      => $request->email,
            'password'   => Hash::make($request->password),
            'role'       => $request->role,
        ]);

        $investor = Investor::create([
            'user_id'                   => $user->id,
            'business_category_id'      => $request->business_category,
            'business_type_id'          => $request->business_type,
            'funding_type_id'           => $request->funding_type,
            'preferred_valuation_range' => $request->preferred_valuation_range,
            'qualified_investor'        => $request->has('qualified_investor') ? 1 : 0
        ]);

        $files = $request->file('files');
        foreach ($files as $key => $file) {
            $docName  = $file->getClientOriginalName();
            $fileName = time() . $key . '.' . $file->extension();
            $filePath = storage_path('app/public') . '/investor-documents/';
            $file->move($filePath, $fileName);

            $investorDocuments = InvestorDocument::create([
                'user_id'   => $user->id,
                'file_name' => $docName,
                'file_path' => '/investor-documents/'.$fileName,
            ]);
        }

        return redirect()->route('admin.users.index')->with('success', 'User added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($role, $id)
    {
        $user = User::where('id', $id)->first();
        if (!$user) {
            abort(404);
        }

        $investor = [];
        $boardMember = [];
        if ($role == 'investor' || $role == 'board_member') {
            $investor = Investor::where('user_id', $user->id)->first();
            if ($user->role == 'board_member' || $user->is_board_member) {
                $boardMember = BoardMember::where('user_id', $user->id)->first();
            }
        }

        return view('admin.users.view', compact('user', 'role', 'investor', 'boardMember'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->first();

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'     => 'required',
            'email'    => 'required',
        ]);

        User::where('id', $id)->update([
            'name'     => $request->name,
            'email'    => $request->email,
            'password' => isset($request->password) ? Hash::make($request->password) : '',
        ]);

        return redirect()->route('admin.users.index')->with('success', 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->delete();

        return back()->with('success', 'User deleted successfully.');
    }

    public function approveUser($id)
    {
        $user = User::where('id', $id)->first();
        $role = ucwords(str_replace('_', ' ', $user->role));
        $user->update([
            'is_approved' => 1
        ]);

        return back()->with('success', 'Successfully approved as qualified investor.');
    }

    public function approveAsBoardMember($id)
    {
        $user = User::where('id', $id)->first();
        $user->update([
            'role'        => 'board_member',
            'is_approved' => 1
        ]);

        return back()->with('success', 'Successfully approved as board member.');
    }
}
