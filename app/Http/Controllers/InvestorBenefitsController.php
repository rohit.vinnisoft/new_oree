<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InvestorBenefit;

class InvestorBenefitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $investorBenefits = InvestorBenefit::all();

        return view('admin.attributes.investor-benefits.index', compact('investorBenefits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.attributes.investor-benefits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $investorBenefit = InvestorBenefit::create([
            'title' => $request->title
        ]);

        return redirect()->route('admin.attributes.investor-benefits.index')->with('success', 'Investor Benefit created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $investorBenefit = InvestorBenefit::where('id', $id)->first();

        return view('admin.attributes.investor-benefits.edit', compact('investorBenefit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $investorBenefit = InvestorBenefit::where('id', $id)->update([
            'title' => $request->title
        ]);

        return redirect()->route('admin.attributes.investor-benefits.index')->with('success', 'Investor Benefit updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $investorBenefit = InvestorBenefit::where('id', $id)->first();
        $investorBenefit->delete();

        return back()->with('success', 'Investor Benefit deleted successfully.');
    }
}
