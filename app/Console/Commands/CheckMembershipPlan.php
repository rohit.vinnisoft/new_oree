<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\UsersMembershipPlan;
use Carbon\Carbon;

class CheckMembershipPlan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:membership-plan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and expripe the membership plan when its time is complete.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $usersMembershipPlans = UsersMembershipPlan::where('expired_at', NULL)->get();
        $now = Carbon::now()->startOfDay();

        foreach ($usersMembershipPlans as $usersMembershipPlan) {
            $planDate = $usersMembershipPlan->created_at;

            if ($usersMembershipPlan->membership_type == 'free') {
                $days = 14;
            }

            if ($usersMembershipPlan->membership_type == 'monthly') {
                $days = 30;
            }

            if ($usersMembershipPlan->membership_type == 'yearly') {
                $days = 365;
            }

            $planExpDate = $planDate->addDays($days)->startOfDay();
            if ($planExpDate < $now) {
                $usersMembershipPlan->update([
                    'expired_at' => Carbon::now()
                ]);
            }
        }
    }
}
