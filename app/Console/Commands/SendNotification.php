<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Notification;

class SendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notification {user_id} {msg} {deal_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to send notifications.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $authUser = Auth::user();
        $msg      = $this->argument('msg');
        $userId   = $this->argument('user_id');
        $dealId   = $this->argument('deal_id');
        $user     = User::where('id', $userId)->where('device_token', '!=', NULL)->first();
        if ($user) {
            // API URL of FCM
            if (strlen($user->device_token) > 50) {
                $url = 'https://fcm.googleapis.com/fcm/send';
                /* api_key available in:Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key */
                $api_key = env('FCM_SERVER_KEY');
                $fields = [
                    "to" => $user->device_token,
                    "collapse_key" => "type_a",
                    "notification" => [
                        "body"  => $msg,
                        "title" => $user->name,
                        "sound" => "default",
                    ],
                    "data" => [
                        "body"           => "Body of Your Notification in Data",
                        "title"          => "Title of Your Notification in Title",
                        "sender_name"    => $user->name,
                        // "sender_picture" => $user->picture,
                    ]
                ];
                // header includes Content type and api key
                $headers = array(
                    'Content-Type:application/json',
                    'Authorization:key='.$api_key
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                // dd($result);
                if ($result === FALSE) {
                    die('FCM Send Error: ' . curl_error($ch));
                }
                curl_close($ch);

                $notification = Notification::where('send_to', $userId)
                                            ->where('send_by', $authUser->id)
                                            ->where('msg', $msg)
                                            ->where('deal_id', $dealId)
                                            ->first();
                if (!$notification) {
                    $notification = Notification::create([
                        'send_to' => $userId,
                        'send_by' => $authUser->id,
                        'msg'     => $msg,
                        'deal_id' => $dealId,
                    ]);
                }

            } else {
                print ('device token not found.'.'</br>');
            }
        } else {
            print ('device token not found.'.'</br>');
            // dd('user not found.');
        }
    }
}
