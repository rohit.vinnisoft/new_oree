<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use App\Models\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = [
            [
                'name'     => 'Admin',
                'email'    => 'admin@admin.com',
                'password' => '123456',
            ],
            [
                'name'     => 'Elgin Amboree',
                'email'    => 'elgin.amboree@oreegroup.com',
                'password' => '123456',
            ]
        ];

        foreach ($admins as $admin) {
            $user = User::where('email', $admin['email'])->first();
            if (!$user) {
                User::create([
                    'name'     => $admin['name'],
                    'email'    => $admin['email'],
                    'password' => Hash::make($admin['password']),
                    'role'     => 'admin'
                ]);
            }
        }
    }
}
