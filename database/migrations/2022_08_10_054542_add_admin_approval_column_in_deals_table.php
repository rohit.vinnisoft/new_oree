<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminApprovalColumnInDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deals', function (Blueprint $table) {
            $table->integer('equity')->nullable();
            $table->integer('admin_approval')->default(0)->comment('0 = Pending, 1 = Accepted, 2 = Rejected');
            $table->text('rejection_reason')->nullable();
            $table->integer('interest_count')->default(0);
            $table->integer('not_interested_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deals', function (Blueprint $table) {
            $table->dropColumn(['equity', 'admin_approval', 'rejection_reason', 'interest_count', 'not_interested_count']);
        });
    }
}
