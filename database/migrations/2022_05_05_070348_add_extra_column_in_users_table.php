<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraColumnInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->after('name')->nullable();
            $table->string('last_name')->after('first_name')->nullable();
            $table->string('role')->nullable();
            $table->text('profile_image')->nullable();
            $table->string('login_type')->default('manual');
            $table->string('social_provider')->nullable();
            $table->string('social_id')->nullable();
            $table->string('deviceType')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'first_name',
                'last_name',
                'role',
                'profile_image',
                'login_type',
                'social_provider',
                'social_id',
                'deviceType',
                'longitude',
                'latitude'
            ]);
        });
    }
}
