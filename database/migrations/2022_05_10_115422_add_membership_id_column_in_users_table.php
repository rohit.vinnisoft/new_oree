<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMembershipIdColumnInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('membership_id')->nullable();
            $table->string('membership_type')->nullable();
            $table->string('company_name')->nullable();
            $table->boolean('is_approved')->default(1);
            $table->text('description')->nullable();
            $table->string('cover_image')->nullable();
            $table->boolean('is_private')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'membership_id',
                'membership_type',
                'company_name',
                'is_approved',
                'description',
                'cover_image',
                'is_private'
            ]);
        });
    }
}
