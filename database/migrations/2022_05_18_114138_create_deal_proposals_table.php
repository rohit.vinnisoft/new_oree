<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_proposals', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('user_role');
            $table->integer('deal_id');
            $table->integer('bid_amount');
            $table->integer('equity');
            $table->enum('status', ['0', '1', '2'])->default(0)->comment('0 = Pending, 1 = Accepted, 2 = Rejected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_proposals');
    }
}
