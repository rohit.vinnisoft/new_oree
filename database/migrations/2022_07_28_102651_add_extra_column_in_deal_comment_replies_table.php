<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraColumnInDealCommentRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deal_comment_replies', function (Blueprint $table) {
            $table->integer('reply_by')->after('deal_comment_id');
            $table->integer('reply_to')->after('reply_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deal_comment_replies', function (Blueprint $table) {
            $table->dropColumn(['reply_by', 'reply_to']);
        });
    }
}
